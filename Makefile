.PHONY: help
.DEFAULT_GOAL = help

DOCKER_COMPOSE=@docker-compose
DOCKER_COMPOSE_EXEC=$(DOCKER_COMPOSE) exec
PHP_DOCKER_COMPOSE_EXEC=$(DOCKER_COMPOSE_EXEC) php
COMPOSER=$(PHP_DOCKER_COMPOSE_EXEC) php -d memory_limit=-1 /usr/local/bin/composer
SYMFONY_CONSOLE=$(PHP_DOCKER_COMPOSE_EXEC) bin/console

## —— Docker 🐳  ———————————————————————————————————————————————————————————————
generate: install db

install:
	$(DOCKER_COMPOSE) build --pull --no-cache
	$(DOCKER_COMPOSE) up -d
	$(SYMFONY_CONSOLE) lexik:jwt:generate-keypair --skip-if-exists

build:
	$(DOCKER_COMPOSE) up --build

start:	## Lancer les containers docker
	$(DOCKER_COMPOSE) up -d

stop:	## Arréter les containers docker
	$(DOCKER_COMPOSE) stop

rm:	stop ## Supprimer les containers docker
	$(DOCKER_COMPOSE) rm -f

restart: rm start	## redémarrer les containers

ssh-php:	## Connexion au container php
	$(PHP_DOCKER_COMPOSE_EXEC) sh

## —— Symfony 🎶 ———————————————————————————————————————————————————————————————
console:
	$(SYMFONY_CONSOLE)

vendor-install:	## Installation des vendors
	$(COMPOSER) install

vendor-update:	## Mise à jour des vendors
	$(COMPOSER) update

clean-vendor: cc-hard ## Suppression du répertoire vendor puis un réinstall
	$(PHP_DOCKER_COMPOSE_EXEC) rm -Rf vendor
	$(PHP_DOCKER_COMPOSE_EXEC) rm composer.lock
	$(COMPOSER) install

cc:	## Vider le cache
	$(SYMFONY_CONSOLE) cache:clear
	$(SYMFONY_CONSOLE) doctrine:cache:clear-metadata
	$(SYMFONY_CONSOLE) doctrine:cache:clear-query
	$(SYMFONY_CONSOLE) doctrine:cache:clear-result

cc-test:	## Vider le cache de l'environnement de test
	$(SYMFONY_CONSOLE) cache:clear --env=test
	$(SYMFONY_CONSOLE) doctrine:cache:clear-metadata --env=test
	$(SYMFONY_CONSOLE) doctrine:cache:clear-query --env=test
	$(SYMFONY_CONSOLE) doctrine:cache:clear-result --env=test

cc-hard: ## Supprimer le répertoire cache
	$(PHP_DOCKER_COMPOSE_EXEC) rm -fR var/cache/*

db: ## Réinitialiser la base de donnée
	$(SYMFONY_CONSOLE) d:d:d --if-exists --force
	$(SYMFONY_CONSOLE) d:d:c
	$(SYMFONY_CONSOLE) d:m:m --no-interaction
	$(SYMFONY_CONSOLE) hautelook:fixtures:load --no-interaction

migration:	## Générer un fichier de migration
	$(SYMFONY_CONSOLE) make:migration

migrate:	## Lancer les migrations
	$(SYMFONY_CONSOLE) do:mi:mi --no-interaction

db-test: ## Réinitialiser la base de donnée en environnement de test
	$(SYMFONY_CONSOLE) d:d:d --if-exists --force --env=test
	$(SYMFONY_CONSOLE) d:d:c --env=test
	$(SYMFONY_CONSOLE) d:m:m --no-interaction --env=test
	$(SYMFONY_CONSOLE) hautelook:fixtures:load --no-interaction --env=test

test: ## Lancement des tests
	$(PHP_DOCKER_COMPOSE_EXEC) bin/phpunit

fixer: ## Lancement php-cs-fixer
	$(PHP_DOCKER_COMPOSE_EXEC) ./tools/php-cs-fixer/vendor/bin/php-cs-fixer fix src

## —— Others 🛠️️ ———————————————————————————————————————————————————————————————
help: ## Liste des commandes
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
