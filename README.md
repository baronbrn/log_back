# Logger

Api for Logger Project

### Get the app ready

> git clone git@gitlab.com:baronbrn/log_back.git
>
> cd log_back ;
> 
> docker-compose up --build ;

### API web Interface

> https://localhost/docs

### Access database

> docker-compose exec database psql -U brnuser -d logger
> 
> Show tables : \dt
> 
> Show table attribute : \d public.user
> 
> Select table entries: select * from public.user;
> 
> Exit database: exit

### ShutDown backend

> Shutdown all containers : docker stop $(docker ps -a -q);
> 
> Clean containers : docker system prune -f;

## Database architecture
![](../../../../../Desktop/MCD_LOGGER_V1.png)
