<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220730143434 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE refresh_tokens_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE asset (id SERIAL NOT NULL, name VARCHAR(255) NOT NULL, current_price DOUBLE PRECISION DEFAULT NULL, symbol VARCHAR(255) DEFAULT NULL, uuid UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2AF5A5CD17F50A6 ON asset (uuid)');
        $this->addSql('COMMENT ON COLUMN asset.uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE category (id SERIAL NOT NULL, name VARCHAR(255) DEFAULT NULL, uuid UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_64C19C1D17F50A6 ON category (uuid)');
        $this->addSql('COMMENT ON COLUMN category.uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE platform (id SERIAL NOT NULL, name VARCHAR(255) NOT NULL, link VARCHAR(255) NOT NULL, uuid UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3952D0CBD17F50A6 ON platform (uuid)');
        $this->addSql('COMMENT ON COLUMN platform.uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE refresh_tokens (id INT NOT NULL, refresh_token VARCHAR(128) NOT NULL, username VARCHAR(255) NOT NULL, valid TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9BACE7E1C74F2195 ON refresh_tokens (refresh_token)');
        $this->addSql('CREATE TABLE rules (id SERIAL NOT NULL, setup_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, content TEXT DEFAULT NULL, uuid UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_899A993CD17F50A6 ON rules (uuid)');
        $this->addSql('CREATE INDEX IDX_899A993CCDCDB68E ON rules (setup_id)');
        $this->addSql('COMMENT ON COLUMN rules.uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE setup (id SERIAL NOT NULL, trader_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, uuid UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_251D5630D17F50A6 ON setup (uuid)');
        $this->addSql('CREATE INDEX IDX_251D56301273968F ON setup (trader_id)');
        $this->addSql('COMMENT ON COLUMN setup.uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE trade (id SERIAL NOT NULL, asset_id INT DEFAULT NULL, category_id INT DEFAULT NULL, trader_id INT DEFAULT NULL, setup_id INT DEFAULT NULL, platform_id INT DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, entry DOUBLE PRECISION DEFAULT NULL, stop_loss DOUBLE PRECISION NOT NULL, take_profit DOUBLE PRECISION DEFAULT NULL, needed_input DOUBLE PRECISION NOT NULL, timeframe VARCHAR(255) DEFAULT NULL, start_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, end_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, win DOUBLE PRECISION NOT NULL, loss DOUBLE PRECISION NOT NULL, passed BOOLEAN DEFAULT NULL, status VARCHAR(255) DEFAULT NULL, wallet_balance DOUBLE PRECISION DEFAULT NULL, progression DOUBLE PRECISION DEFAULT NULL, risk_reward DOUBLE PRECISION DEFAULT NULL, risk_allowed DOUBLE PRECISION DEFAULT NULL, uuid UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7E1A4366D17F50A6 ON trade (uuid)');
        $this->addSql('CREATE INDEX IDX_7E1A43665DA1941 ON trade (asset_id)');
        $this->addSql('CREATE INDEX IDX_7E1A436612469DE2 ON trade (category_id)');
        $this->addSql('CREATE INDEX IDX_7E1A43661273968F ON trade (trader_id)');
        $this->addSql('CREATE INDEX IDX_7E1A4366CDCDB68E ON trade (setup_id)');
        $this->addSql('CREATE INDEX IDX_7E1A4366FFE6496F ON trade (platform_id)');
        $this->addSql('COMMENT ON COLUMN trade.uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE "user" (id SERIAL NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, uuid UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649D17F50A6 ON "user" (uuid)');
        $this->addSql('COMMENT ON COLUMN "user".uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE rules ADD CONSTRAINT FK_899A993CCDCDB68E FOREIGN KEY (setup_id) REFERENCES setup (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE setup ADD CONSTRAINT FK_251D56301273968F FOREIGN KEY (trader_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE trade ADD CONSTRAINT FK_7E1A43665DA1941 FOREIGN KEY (asset_id) REFERENCES asset (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE trade ADD CONSTRAINT FK_7E1A436612469DE2 FOREIGN KEY (category_id) REFERENCES category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE trade ADD CONSTRAINT FK_7E1A43661273968F FOREIGN KEY (trader_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE trade ADD CONSTRAINT FK_7E1A4366CDCDB68E FOREIGN KEY (setup_id) REFERENCES setup (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE trade ADD CONSTRAINT FK_7E1A4366FFE6496F FOREIGN KEY (platform_id) REFERENCES platform (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE trade DROP CONSTRAINT FK_7E1A43665DA1941');
        $this->addSql('ALTER TABLE trade DROP CONSTRAINT FK_7E1A436612469DE2');
        $this->addSql('ALTER TABLE trade DROP CONSTRAINT FK_7E1A4366FFE6496F');
        $this->addSql('ALTER TABLE rules DROP CONSTRAINT FK_899A993CCDCDB68E');
        $this->addSql('ALTER TABLE trade DROP CONSTRAINT FK_7E1A4366CDCDB68E');
        $this->addSql('ALTER TABLE setup DROP CONSTRAINT FK_251D56301273968F');
        $this->addSql('ALTER TABLE trade DROP CONSTRAINT FK_7E1A43661273968F');
        $this->addSql('DROP SEQUENCE refresh_tokens_id_seq CASCADE');
        $this->addSql('DROP TABLE asset');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE platform');
        $this->addSql('DROP TABLE refresh_tokens');
        $this->addSql('DROP TABLE rules');
        $this->addSql('DROP TABLE setup');
        $this->addSql('DROP TABLE trade');
        $this->addSql('DROP TABLE "user"');
    }
}
