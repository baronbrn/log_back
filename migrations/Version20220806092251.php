<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220806092251 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE wallet (id SERIAL NOT NULL, category_id INT DEFAULT NULL, trader_id INT DEFAULT NULL, balance DOUBLE PRECISION DEFAULT NULL, uuid UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7C68921FD17F50A6 ON wallet (uuid)');
        $this->addSql('CREATE INDEX IDX_7C68921F12469DE2 ON wallet (category_id)');
        $this->addSql('CREATE INDEX IDX_7C68921F1273968F ON wallet (trader_id)');
        $this->addSql('COMMENT ON COLUMN wallet.uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE wallet ADD CONSTRAINT FK_7C68921F12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE wallet ADD CONSTRAINT FK_7C68921F1273968F FOREIGN KEY (trader_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE wallet');
    }
}
