<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220821164800 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE notification (id SERIAL NOT NULL, trader_id INT DEFAULT NULL, trade_id INT DEFAULT NULL, content TEXT NOT NULL, read BOOLEAN NOT NULL, uuid UUID NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BF5476CAD17F50A6 ON notification (uuid)');
        $this->addSql('CREATE INDEX IDX_BF5476CA1273968F ON notification (trader_id)');
        $this->addSql('CREATE INDEX IDX_BF5476CAC2D9760 ON notification (trade_id)');
        $this->addSql('COMMENT ON COLUMN notification.uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CA1273968F FOREIGN KEY (trader_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CAC2D9760 FOREIGN KEY (trade_id) REFERENCES trade (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE setup ADD created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE trade ADD created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE "user" RENAME COLUMN registered_at TO created_at');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE notification');
        $this->addSql('ALTER TABLE setup DROP created_at');
        $this->addSql('ALTER TABLE trade DROP created_at');
        $this->addSql('ALTER TABLE "user" RENAME COLUMN created_at TO registered_at');
    }
}
