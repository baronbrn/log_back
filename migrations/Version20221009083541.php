<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221009083541 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE parameter (id SERIAL NOT NULL, currency VARCHAR(255) NOT NULL, language VARCHAR(255) NOT NULL, uuid UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2A979110D17F50A6 ON parameter (uuid)');
        $this->addSql('COMMENT ON COLUMN parameter.uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE "user" ADD parameter_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT FK_8D93D6497C56DBD6 FOREIGN KEY (parameter_id) REFERENCES parameter (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6497C56DBD6 ON "user" (parameter_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT FK_8D93D6497C56DBD6');
        $this->addSql('DROP TABLE parameter');
        $this->addSql('DROP INDEX UNIQ_8D93D6497C56DBD6');
        $this->addSql('ALTER TABLE "user" DROP parameter_id');
    }
}
