<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221010090349 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE currency (id SERIAL NOT NULL, name VARCHAR(255) NOT NULL, uuid UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6956883FD17F50A6 ON currency (uuid)');
        $this->addSql('COMMENT ON COLUMN currency.uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE parameter ADD currency_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE parameter DROP currency');
        $this->addSql('ALTER TABLE parameter ADD CONSTRAINT FK_2A97911038248176 FOREIGN KEY (currency_id) REFERENCES currency (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_2A97911038248176 ON parameter (currency_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE parameter DROP CONSTRAINT FK_2A97911038248176');
        $this->addSql('DROP TABLE currency');
        $this->addSql('DROP INDEX IDX_2A97911038248176');
        $this->addSql('ALTER TABLE parameter ADD currency VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE parameter DROP currency_id');
    }
}
