<?php

namespace App\DataFixtures;

use App\Entity\Asset;
use Doctrine\Persistence\ObjectManager;

class AssetFixtures extends CommonFixtures
{
    public $assets = [
        'BTC','ETH','BNB','LTC','XRP','SOL','ADA','HBAR','XML','MKR','LINK',
        'SXP','SYS','SNX','MATIC','USDT','USDC','TVK','QNT','ONT'];
    public function load(ObjectManager $manager)
    {
        foreach ($this->assets as $asset) {
            $manager->persist(
                (new Asset())
                    ->setName($asset)
                    ->setCurrentPrice(rand())
                    ->setSymbol($asset)
            );
            $manager->flush();
        }
    }

    public function getOrder(): int
    {
        return 2;
    }
}
