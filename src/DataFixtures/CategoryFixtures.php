<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends CommonFixtures
{
    public function load(ObjectManager $manager)
    {
        $manager->persist((new Category())->setName(Category::SPOT));
        $manager->persist((new Category())->setName(Category::FUTURE));
        $manager->flush();
    }

    public function getOrder(): int
    {
        return 3;
    }
}
