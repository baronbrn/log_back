<?php

namespace App\DataFixtures;

use App\Repository\AssetRepository;
use App\Repository\CategoryRepository;
use App\Repository\PlatformRepository;
use App\Repository\SetupRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use ReflectionClass;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CommonFixtures extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    protected ContainerInterface $container;
    protected SetupRepository $setupRepository;
    protected CategoryRepository $categoryRepository;
    protected AssetRepository $assetRepository;
    protected UserPasswordEncoderInterface $userPasswordEncoder;
    protected UserRepository $userRepository;
    protected PlatformRepository $platformRepository;

    public function __construct(
        SetupRepository $setupRepository,
        CategoryRepository $categoryRepository,
        AssetRepository $assetRepository,
        UserPasswordEncoderInterface $userPasswordEncoder,
        UserRepository $userRepository,
        PlatformRepository $platformRepository
    ) {
        $this->setupRepository = $setupRepository;
        $this->categoryRepository = $categoryRepository;
        $this->assetRepository = $assetRepository;
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->userRepository = $userRepository;
        $this->platformRepository = $platformRepository;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
    }

    public static function getConstantsAsArray(string $class): array
    {
        return array_values((new ReflectionClass($class))->getConstants());
    }

    public function getOrder(): int
    {
        return 1;
    }
}
