<?php

namespace App\DataFixtures;

use App\Entity\Currency;
use Doctrine\Persistence\ObjectManager;

class CurrencyFixtures extends CommonFixtures
{
    public const CURRENCIES = ['Euro', 'Dollar', 'Yuan', 'Rubble', 'Pound', 'Pesos', 'Canadian dollar', 'Dinard'];

    public function load(ObjectManager $manager)
    {
        foreach (self::CURRENCIES as $currency) {
            $manager->persist((new Currency())->setName($currency));
        }
        $manager->flush();
    }

    public function getOrder(): int
    {
        return 8;
    }
}
