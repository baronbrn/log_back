<?php

namespace App\DataFixtures;

use App\Entity\Platform;
use Doctrine\Persistence\ObjectManager;

class PlatformFixtures extends CommonFixtures
{
    public function load(ObjectManager $manager)
    {
        $platforms = ['Binance', 'Okex', 'Huobi', 'Kraken', 'Coinbase', 'FTX'];
        foreach ($platforms as $platformName) {
            $manager->persist(
                (new Platform())->setName($platformName)
            );
        }
        $manager->flush();
    }

    public function getOrder(): int
    {
        return 4;
    }
}
