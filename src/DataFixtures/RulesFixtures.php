<?php

namespace App\DataFixtures;

use App\Entity\Rules;
use Doctrine\Persistence\ObjectManager;

class RulesFixtures extends CommonFixtures
{
    public function load(ObjectManager $manager)
    {
        $setups = $this->setupRepository->findAll();
        foreach ($setups as $setup) {
            for ($i = 0; $i < 3; $i++) {
                $manager->persist(
                    (new Rules())
                        ->setName('rules n°' . $i)
                        ->setContent('content of rule')
                        ->setSetup($setup)
                );
            }
        }
        $manager->flush();
    }

    public function getOrder(): int
    {
        return 7;
    }
}
