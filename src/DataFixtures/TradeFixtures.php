<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Trade;
use App\Entity\User;
use App\Utils\Utils;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Persistence\ObjectManager;

class TradeFixtures extends CommonFixtures
{
    public function load(ObjectManager $manager)
    {
        foreach ($this->userRepository->findAll() as $user) {
            $this->loadTrade($manager, $user);
        }
        $manager->flush();
    }

    public function loadTrade(ObjectManager $manager, User $trader)
    {
        $statusList = [Trade::STATUS_IN_PROGRESS, Trade::STATUS_LOSS, Trade::STATUS_WIN, Trade::STATUS_NEUTRAL];
        $assets = $this->assetRepository->findAll();
        $timeFrames = ['15m', '30m', '1h', '2h', '4h', '8h', '12h', '1D', '3D', '1W', '1M'];
        /** @var ArrayCollection $categories */
        $categories = $this->categoryRepository->findAll();
        $userSetup = $this->setupRepository->findBy(['trader' => $trader]);
        $platforms = $this->platformRepository->findAll();

        for ($i = 0; $i < 16; $i++) {
            $type = (rand(0, 1) === 1) ? 'long' : 'short';
            $entry = (rand(0, 10000));
            $riskReward = rand(1, 5);
            $status = $statusList[rand(0, count($statusList) - 1)];

            /** @var Category $category */

            $trade = (new Trade())
                ->setType($type)
                ->setEntry($entry)
                ->setStopLoss(($type === 'long') ? ($entry * (20 / 100)) - $entry : ($entry * (20 / 100)) + $entry)
                ->setTakeProfit(($type === 'long') ?
                    (($entry * (20 / 100)) * 2) + $entry :
                    (($entry * (20 / 100)) * 2) - $entry)
                ->setNeededInput(rand(10, 10000))
                ->setTimeframe($timeFrames[rand(0, count($timeFrames) - 1)])
                ->setStartDate($start = (new \DateTime('-' . rand(0, 5) . ' days')))
                ->setEndDate((new \DateTime(Utils::dateToString($start)))->modify('+' . rand(0, 5) . 'days'))
                ->setWin($win = rand(20, 1000))
                ->setLoss($win / $riskReward)
                ->setPassed(rand(0, 1) === 1)
                ->setStatus($status)
                ->setWalletBalance(200)
                ->setProgression(20.5)
                ->setRiskReward($riskReward . ':1')
                ->setCategory($category = (rand(0, 1) === 1) ? $categories[0] : $categories[1])
                ->setLeverage($category->isFuture() ? rand(1, 150) : null)
                ->setAsset($assets[rand(0, sizeof($assets) - 1)])
                ->setTrader($trader)
                ->setSetup($userSetup[rand(0, sizeof($userSetup) - 1)])
                ->setPlatform($platforms[rand(0, sizeof($platforms) - 1)])
            ;

            $manager->persist($trade);
        }
    }

    public function getOrder(): int
    {
        return 6;
    }
}
