<?php

namespace App\DataFixtures;

use App\Entity\Setup;
use App\Entity\User;
use App\Generic\Role;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends CommonFixtures
{
    public const DEFAULT_PASSWORD = 'password';
    public const EXAMPLE_RISK_ALLOWED = ['2:1', '3:1', '4:1', '14:1'];

    public function load(ObjectManager $manager)
    {
        $this->createAdmin($manager);
        $this->createRandomTrader($manager);
        $manager->flush();
    }

    public function createAdmin(ObjectManager $manager)
    {
        $manager->persist($this->loadUser('admin@gmail.com', [Role::ADMIN]));
    }

    public function createRandomTrader(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $manager->persist($this->loadUser('trader' . $i . '@gmail.com', [Role::TRADER]));
        }
    }

    public function loadUser(string $email, array $roles, ?string $password = null): User
    {
        $user = (new User())
            ->setEmail($email)
            ->setRoles($roles)
            ->setUsername('some username')
            ->setBirthdate((new \DateTime())->modify('-' . rand(16, 40) . ' years'))
            ->setFirstName('firstname')
            ->setLastName('lastname')
            ->setPhone('06455676' . rand(10, 30))
            ->setCountry('France')
            ->setGender(User::GENDER_MALE);

        $user->setPassword($this->userPasswordEncoder->encodePassword($user, $password ?: self::DEFAULT_PASSWORD));
        for ($i = 0; $i < 3; $i++) {
            $user->addSetup(
                (new Setup())
                    ->setName('random name')
                    ->setDescription('Some description')
                    ->setRiskAllowed(self::EXAMPLE_RISK_ALLOWED[rand(0, sizeof(self::EXAMPLE_RISK_ALLOWED) - 1)])
            );
        }
        return $user;
    }

    public function getOrder(): int
    {
        return 5;
    }
}
