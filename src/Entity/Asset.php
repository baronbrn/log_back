<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Generic\Group;
use App\Repository\AssetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\IdentifierTraits;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: AssetRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get" => [
            "security" => "is_granted('ROLE_USER')"
        ],
        "post" => [
            "security" => "is_granted('ROLE_ADMIN')"
        ]
    ],
    itemOperations: [
        "get" => [
            "security" => "is_granted('ROLE_USER', object)"
        ],
        "put" => [
            "security" => "is_granted('ROLE_ADMIN')"
        ],
        "delete" => [
            "security" => "is_granted('ROLE_ADMIN')"
        ]
    ],
    denormalizationContext: ['groups' => [Group::ASSET_POST, Group::ASSET_PUT]],
    normalizationContext: ['groups' => [Group::ASSET_GET]],
)]
class Asset
{
    use IdentifierTraits;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups([Group::ASSET_GET, Group::ASSET_PUT, Group::ASSET_POST, Group::TRADE_GET, Group::USER_GET])]
    private ?string $name;

    #[ORM\Column(type: 'float', nullable: true)]
    #[Groups([Group::ASSET_GET, Group::ASSET_PUT, Group::ASSET_POST, Group::TRADE_GET])]
    private ?float $currentPrice;

    #[ORM\OneToMany(mappedBy: 'asset', targetEntity: Trade::class)]
    #[Groups([Group::ASSET_GET])]
    private $trades;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups([Group::ASSET_GET, Group::ASSET_PUT, Group::ASSET_POST, Group::TRADE_GET])]
    private ?string $symbol;

    public function __construct()
    {
        $this->trades = new ArrayCollection();
        $this->uuid = $this->generateUuid();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCurrentPrice(): ?float
    {
        return $this->currentPrice;
    }

    public function setCurrentPrice(?float $currentPrice): self
    {
        $this->currentPrice = $currentPrice;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getTrades(): Collection
    {
        return $this->trades;
    }

    public function addTrade(Trade $trade): self
    {
        if (!$this->trades->contains($trade)) {
            $this->trades[] = $trade;
            $trade->setAsset($this);
        }

        return $this;
    }

    public function removeTrade(Trade $trade): self
    {
        if ($this->trades->removeElement($trade)) {
            // set the owning side to null (unless already changed)
            if ($trade->getAsset() === $this) {
                $trade->setAsset(null);
            }
        }

        return $this;
    }

    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    public function setSymbol(?string $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }
}
