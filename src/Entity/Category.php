<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\IdentifierTraits;
use App\Generic\Group;
use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get" => [],
        "post" => [
            "security" => "is_granted('ROLE_ADMIN')"
        ]
    ],
    itemOperations: [
        "get" => [],
        "put" => [
            "security" => "is_granted('ROLE_ADMIN')"
        ],
        "delete" => [
            "security" => "is_granted('ROLE_ADMIN')"
        ]
    ],
    denormalizationContext: ['groups' => [Group::CATEGORY_POST, Group::CATEGORY_PUT]],
    normalizationContext: ['groups' => [Group::CATEGORY_GET]],
)]
class Category
{
    use IdentifierTraits;

    public const SPOT = 'Spot';
    public const FUTURE = 'Future';

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups([
        Group::CATEGORY_GET,Group::CATEGORY_POST, Group::CATEGORY_PUT,
        Group::USER_GET, Group::TRADE_GET, Group::USER_GET
    ])]
    private $name;

    #[ORM\OneToMany(mappedBy: 'category', targetEntity: Trade::class)]
    private $trades;

    #[ORM\OneToMany(mappedBy: 'category', targetEntity: Wallet::class)]
    private $wallets;

    public function __construct()
    {
        $this->uuid = $this->generateUuid();
        $this->trades = new ArrayCollection();
        $this->wallets = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Trade>
     */
    public function getTrades(): Collection
    {
        return $this->trades;
    }

    public function addTrade(Trade $trade): self
    {
        if (!$this->trades->contains($trade)) {
            $this->trades[] = $trade;
            $trade->setCategory($this);
        }

        return $this;
    }

    public function removeTrade(Trade $trade): self
    {
        if ($this->trades->removeElement($trade)) {
            // set the owning side to null (unless already changed)
            if ($trade->getCategory() === $this) {
                $trade->setCategory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Wallet>
     */
    public function getWallets(): Collection
    {
        return $this->wallets;
    }

    public function addWallet(Wallet $wallet): self
    {
        if (!$this->wallets->contains($wallet)) {
            $this->wallets[] = $wallet;
            $wallet->setCategory($this);
        }

        return $this;
    }

    public function removeWallet(Wallet $wallet): self
    {
        if ($this->wallets->removeElement($wallet)) {
            // set the owning side to null (unless already changed)
            if ($wallet->getCategory() === $this) {
                $wallet->setCategory(null);
            }
        }

        return $this;
    }

    public function isFuture(): bool
    {
        return $this->getName() === self::FUTURE;
    }
    public function isSpot(): bool
    {
        return $this->getName() === self::SPOT;
    }
}
