<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\IdentifierTraits;
use App\Generic\Group;
use App\Repository\CurrencyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotBlank;

#[ORM\Entity(repositoryClass: CurrencyRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get" => [
            "security" => "is_granted('ROLE_TRADER')"
        ],
        "post" => [
            "security" => "is_granted('ROLE_ADMIN')",
        ]
    ],
    itemOperations: [
        "get" => [
            "security" => "is_granted('ROLE_TRADER')"
        ],
    ],
    denormalizationContext: ['groups' => [Group::CURRENCY_POST]],
    normalizationContext: ['groups' => [Group::CURRENCY_GET]]
)]
class Currency
{
    use IdentifierTraits;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups([Group::CURRENCY_GET, Group::CURRENCY_POST, Group::PARAMETER_GET])]
    #[NotBlank(message: 'The field "name" of a currency can not be null')]
    private $name;

    #[ORM\OneToMany(mappedBy: 'currency', targetEntity: Parameter::class)]
    private $parameters;

    public function __construct()
    {
        $this->uuid = $this->generateUuid();
        $this->parameters = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Parameter>
     */
    public function getParameters(): Collection
    {
        return $this->parameters;
    }

    public function addParameter(Parameter $parameter): self
    {
        if (!$this->parameters->contains($parameter)) {
            $this->parameters[] = $parameter;
            $parameter->setCurrency($this);
        }

        return $this;
    }

    public function removeParameter(Parameter $parameter): self
    {
        if ($this->parameters->removeElement($parameter)) {
            // set the owning side to null (unless already changed)
            if ($parameter->getCurrency() === $this) {
                $parameter->setCurrency(null);
            }
        }

        return $this;
    }
}
