<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\IdentifierTraits;
use App\Entity\Traits\TimestampTraits;
use App\Generic\Group;
use App\Repository\NotificationRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: NotificationRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get" => [],
        "post" => []
    ],
    itemOperations: [
        "get" => [
            "security" => "is_granted('GET_NOTIFICATION', object)"
        ],
        "put" => [
            "security" => "is_granted('PUT_NOTIFICATION', object)"
        ],
        "delete" => [
            "security" => "is_granted('DELETE_NOTIFICATION', object)"
        ]
    ],
    denormalizationContext: ['groups' => [Group::NOTIFICATION_POST, Group::NOTIFICATION_PUT]],
    normalizationContext: ['groups' => [Group::NOTIFICATION_GET]]
)]
#[HasLifecycleCallbacks]
class Notification
{
    use IdentifierTraits;
    use TimestampTraits;

    #[ORM\Column(type: 'text')]
    #[Groups([Group::NOTIFICATION_GET, Group::NOTIFICATION_PUT, Group::NOTIFICATION_POST])]
    private $content;

    #[ORM\Column(type: 'boolean')]
    #[Groups([Group::NOTIFICATION_GET, Group::NOTIFICATION_PUT, Group::NOTIFICATION_POST])]
    private $read = false;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'notifications')]
    private $trader;

    #[ORM\ManyToOne(targetEntity: Trade::class, inversedBy: 'notifications')]
    private $trade;

    public function __construct()
    {
        $this->uuid = $this->generateUuid();
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getRead(): ?bool
    {
        return $this->read;
    }

    public function setRead(bool $read): self
    {
        $this->read = $read;

        return $this;
    }

    public function getTrader(): ?User
    {
        return $this->trader;
    }

    public function setTrader(?User $trader): self
    {
        $this->trader = $trader;

        return $this;
    }

    public function getTrade(): ?Trade
    {
        return $this->trade;
    }

    public function setTrade(?Trade $trade): self
    {
        $this->trade = $trade;

        return $this;
    }
}
