<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\IdentifierTraits;
use App\Generic\Group;
use App\Generic\ValidationGroup;
use App\Repository\ParameterRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity(repositoryClass: ParameterRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get" => [
            "security" => "is_granted('ROLE_TRADER')"
        ],
    ],
    itemOperations: [
        "get" => [
            "security" => "is_granted('GET_PARAMETER', object)"
        ],
        "put" => [
            "security" => "is_granted('PUT_PARAMETER', object)",
            'validation_groups' => [ValidationGroup::PUT_PARAMETER]
        ]
    ],
    denormalizationContext: ['groups' => [Group::PARAMETER_PUT]],
    normalizationContext: ['groups' => [Group::PARAMETER_GET]]
)]
class Parameter
{
    use IdentifierTraits;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups([Group::PARAMETER_GET, Group::PARAMETER_PUT, Group::USER_GET])]
    #[NotBlank(message: 'The language can not be null', groups: [ValidationGroup::PUT_PARAMETER])]
    private $language;

    #[ORM\OneToOne(mappedBy: 'parameter', targetEntity: User::class, cascade: ['persist', 'remove'])]
    private $trader;

    #[ORM\ManyToOne(targetEntity: Currency::class, inversedBy: 'parameters')]
    #[Groups([Group::PARAMETER_GET, Group::PARAMETER_PUT, Group::USER_GET])]
    private $currency;

    public function __construct()
    {
        $this->uuid = $this->generateUuid();
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getTrader(): ?User
    {
        return $this->trader;
    }

    public function setTrader(?User $trader): self
    {
        // unset the owning side of the relation if necessary
        if ($trader === null && $this->trader !== null) {
            $this->trader->setParameter(null);
        }

        // set the owning side of the relation if necessary
        if ($trader !== null && $trader->getParameter() !== $this) {
            $trader->setParameter($this);
        }

        $this->trader = $trader;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }
}
