<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\IdentifierTraits;
use App\Generic\Group;
use App\Repository\PlatformRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: PlatformRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get" => [],
        "post" => []
    ],
    itemOperations: [
        "get" => [],
        "put" => [],
        "delete" => []
    ],
    denormalizationContext: ['groups' => [Group::PLATFORM_POST]],
    normalizationContext: ['groups' => [Group::PLATFORM_GET]],
    security: "is_granted('ROLE_USER')"
)]
class Platform
{
    use IdentifierTraits;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups([Group::PLATFORM_GET, Group::PLATFORM_POST, Group::TRADE_GET])]
    private ?string $name;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups([Group::PLATFORM_GET, Group::PLATFORM_POST])]
    private ?string $link;

    #[ORM\OneToMany(mappedBy: 'platform', targetEntity: Trade::class)]
    private $trades;

    public function __construct()
    {
        $this->uuid = $this->generateUuid();
        $this->trades = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return Collection<int, Trade>
     */
    public function getTrades(): Collection
    {
        return $this->trades;
    }

    public function addTrade(Trade $trade): self
    {
        if (!$this->trades->contains($trade)) {
            $this->trades[] = $trade;
            $trade->setPlatform($this);
        }

        return $this;
    }

    public function removeTrade(Trade $trade): self
    {
        if ($this->trades->removeElement($trade)) {
            // set the owning side to null (unless already changed)
            if ($trade->getPlatform() === $this) {
                $trade->setPlatform(null);
            }
        }

        return $this;
    }
}
