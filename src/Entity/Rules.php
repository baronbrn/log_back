<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\IdentifierTraits;
use App\Generic\Group;
use App\Repository\RulesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotBlank;

#[ORM\Entity(repositoryClass: RulesRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get" => [
            "security" => "is_granted('ROLE_ADMIN')"
        ],
        "post" => [
            "security" => "is_granted('ROLE_TRADER')"
        ]
    ],
    itemOperations: [
        "get" => [
            "security" => "is_granted('GET_RULES', object)"
        ],
        "put" => [
            "security" => "is_granted('PUT_RULES', object)"
        ],
        "delete" => [
            "security" => "is_granted('DELETE_RULES', object)"
        ]
    ],
    denormalizationContext: ['groups' => [Group::RULES_POST, Group::RULES_PUT]],
    normalizationContext: ['groups' => [Group::RULES_GET]]
)]
class Rules
{
    use IdentifierTraits;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups([Group::RULES_GET, Group::RULES_POST, Group::RULES_PUT, Group::SETUP_GET, Group::SETUP_POST])]
    private $name;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups([Group::RULES_GET, Group::RULES_POST, Group::RULES_PUT, Group::SETUP_GET, Group::SETUP_POST])]
    private $content;

    #[ORM\ManyToOne(targetEntity: Setup::class, inversedBy: 'rules')]
    #[Groups([Group::RULES_GET, Group::RULES_POST])]
    #[NotBlank]
    private $setup;

    public function __construct()
    {
        $this->uuid = $this->generateUuid();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getSetup(): ?Setup
    {
        return $this->setup;
    }

    public function setSetup(?Setup $setup): self
    {
        $this->setup = $setup;

        return $this;
    }
}
