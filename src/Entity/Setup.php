<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Entity\Traits\IdentifierTraits;
use App\Entity\Traits\TimestampTraits;
use App\Generic\Group;
use App\Repository\SetupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity(repositoryClass: SetupRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get" => [],
        "post" => [],
    ],
    itemOperations: [
        "get" => [
            "security" => "is_granted('GET_SETUP', object)"
        ],
        "put" => [
            "security" => "is_granted('PUT_SETUP', object)"
        ],
        "delete" => [
            "security" => "is_granted('DELETE_SETUP', object)"
        ]
    ],
    denormalizationContext: ['groups' => [Group::SETUP_POST, Group::SETUP_PUT]],
    normalizationContext: ['groups' => [Group::SETUP_GET]],
    order: ['createdAt' => 'DESC']
)]
#[ApiFilter(BooleanFilter::class, properties: [
    'favorite'
])]
#[ApiFilter(SearchFilter::class, properties: [
    'name' => 'exact'
])]
#[HasLifecycleCallbacks]
class Setup
{
    use IdentifierTraits;
    use TimestampTraits;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups([Group::SETUP_GET, Group::SETUP_POST, Group::SETUP_PUT, Group::TRADE_GET, Group::USER_GET])]
    #[NotNull(message: "La valeur 'name' ne peut pas être null.")]
    #[NotBlank(message: "La valeur 'name' ne peut pas être null.")]
    #[Length(min: 2, minMessage: "La valeur 'name' doit faire au moins deux charactères")]
    private ?string $name;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups([Group::SETUP_GET, Group::SETUP_POST, Group::SETUP_PUT])]
    private ?string $description;

    #[ORM\OneToMany(mappedBy: 'setup', targetEntity: Rules::class, cascade: ['persist', 'remove'])]
    #[Groups([Group::SETUP_GET, Group::SETUP_POST])]
    private $rules;

    #[ORM\OneToMany(mappedBy: 'setup', targetEntity: Trade::class)]
    private $trades;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'setups')]
    #[Groups([Group::SETUP_GET, Group::SETUP_POST])]
    #[NotNull]
    #[NotBlank]
    private $trader;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups([Group::SETUP_GET, Group::SETUP_POST, Group::SETUP_PUT])]
    private $riskAllowed;

    #[ORM\Column(type: 'boolean')]
    #[Groups([Group::SETUP_GET, Group::SETUP_POST, Group::SETUP_PUT])]
    private $favorite = false;

    public function __construct()
    {
        $this->uuid = $this->generateUuid();
        $this->rules = new ArrayCollection();
        $this->trades = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, Rules>
     */
    public function getRules(): Collection
    {
        return $this->rules;
    }

    public function addRule(Rules $rule): self
    {
        if (!$this->rules->contains($rule)) {
            $this->rules[] = $rule;
            $rule->setSetup($this);
        }

        return $this;
    }

    public function removeRule(Rules $rule): self
    {
        if ($this->rules->removeElement($rule)) {
            // set the owning side to null (unless already changed)
            if ($rule->getSetup() === $this) {
                $rule->setSetup(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Trade>
     */
    public function getTrades(): Collection
    {
        return $this->trades;
    }

    public function addTrade(Trade $trade): self
    {
        if (!$this->trades->contains($trade)) {
            $this->trades[] = $trade;
            $trade->setSetup($this);
        }

        return $this;
    }

    public function removeTrade(Trade $trade): self
    {
        if ($this->trades->removeElement($trade)) {
            // set the owning side to null (unless already changed)
            if ($trade->getSetup() === $this) {
                $trade->setSetup(null);
            }
        }

        return $this;
    }

    public function getTrader(): ?User
    {
        return $this->trader;
    }

    public function setTrader(?User $trader): self
    {
        $this->trader = $trader;

        return $this;
    }

    public function getRiskAllowed(): ?string
    {
        return $this->riskAllowed;
    }

    public function setRiskAllowed(?string $riskAllowed): self
    {
        $this->riskAllowed = $riskAllowed;

        return $this;
    }

    #[SerializedName('numberOfTrade')]
    #[Groups([Group::SETUP_GET])]
    public function getCountTrade(): int
    {
        return $this->trades->count();
    }

    public function getFavorite(): ?bool
    {
        return $this->favorite;
    }

    public function setFavorite(bool $favorite): self
    {
        $this->favorite = $favorite;

        return $this;
    }

    public function isFavorite(): ?bool
    {
        return $this->getFavorite();
    }
}
