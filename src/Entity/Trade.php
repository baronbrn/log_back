<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Entity\Traits\IdentifierTraits;
use App\Entity\Traits\TimestampTraits;
use App\Generic\Group;
use App\Generic\ValidationGroup;
use App\Repository\TradeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\ManyToOne;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use App\Validator\IsValidTrade;

#[ORM\Entity(repositoryClass: TradeRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get" => [],
        "post" => [
            'validation_groups' => [ValidationGroup::POST_TRADE]
        ]
    ],
    itemOperations: [
        "get" => [
            "security" => "is_granted('GET_TRADE', object)"
        ],
        "put" => [
            "security" => "is_granted('PUT_TRADE', object)",
            'validation_groups' => [ValidationGroup::PUT_TRADE]
        ],
        "delete" => [
            "security" => "is_granted('DELETE_TRADE', object)"
        ]
    ],
    denormalizationContext: ['groups' => [Group::TRADE_POST, Group::TRADE_PUT]],
    normalizationContext: ['groups' => [Group::TRADE_GET]],
    order: ['startDate' => 'DESC']
)]
#[ApiFilter(
    SearchFilter::class,
    properties: ['users' => 'exact', 'status' => 'exact', 'category.name' => 'exact', 'asset.name' => 'exact']
)]
#[ApiFilter(
    BooleanFilter::class,
    properties: [ 'favorite' ]
)]
#[HasLifecycleCallbacks]
#[IsValidTrade(groups: [ValidationGroup::POST_TRADE, ValidationGroup::PUT_TRADE])]
class Trade
{
    use IdentifierTraits;
    use TimestampTraits;

    public const TYPE_LONG = 'long';
    public const TYPE_SHORT = 'short';

    public const STATUS_WIN = 'win';
    public const STATUS_LOSS = 'loss';
    public const STATUS_IN_PROGRESS = 'in progress';
    public const STATUS_NEUTRAL = 'neutral';

    public const DEFAULT_PAIR = 'USDT';

    #[Column(type: 'string', length: 255)]
    #[Groups([Group::TRADE_GET, Group::TRADE_POST, Group::TRADE_PUT, Group::USER_GET])]
    #[Choice([self::TYPE_LONG, self::TYPE_SHORT])]
    #[NotBlank(message: "La valeur 'type' ne peut pas être vide.", groups: [ValidationGroup::POST_TRADE])]
    #[Length(
        min: 2,
        minMessage: "Le champ 'type' doit faire au moins deux charactères.",
        groups: [ValidationGroup::POST_TRADE]
    )]
    private ?string $type;

    #[Column(type: 'float', nullable: false)]
    #[Groups([Group::TRADE_GET, Group::TRADE_POST, Group::TRADE_PUT, Group::USER_GET])]
    #[NotBlank(message: "La valeur 'entry' ne peut pas être vide.", groups: [ValidationGroup::POST_TRADE])]
    private ?float $entry;

    #[Column(type: 'float', nullable: true)]
    #[Groups([Group::TRADE_GET, Group::TRADE_POST, Group::TRADE_PUT, Group::USER_GET])]
    private ?float $stopLoss;

    #[Column(type: 'float', nullable: true)]
    #[Groups([Group::TRADE_GET, Group::TRADE_POST, Group::TRADE_PUT, Group::USER_GET])]
    private ?float $takeProfit;

    #[Column(type: 'float')]
    #[Groups([Group::TRADE_GET, Group::TRADE_POST, Group::TRADE_PUT, Group::USER_GET])]
    private ?float $neededInput = 0;

    #[Column(type: 'string', length: 255, nullable: true)]
    #[Groups([Group::TRADE_GET, Group::TRADE_POST, Group::TRADE_PUT, Group::USER_GET])]
    private ?string $timeframe;

    #[Column(type: 'datetime', nullable: true)]
    #[Groups([Group::TRADE_GET, Group::TRADE_POST, Group::TRADE_PUT])]
    private ?\DateTimeInterface $startDate;

    #[Column(type: 'datetime', nullable: true)]
    #[Groups([Group::TRADE_GET, Group::TRADE_POST, Group::TRADE_PUT])]
    private ?\DateTimeInterface $endDate;

    #[Column(type: 'float')]
    #[Groups([Group::TRADE_GET, Group::TRADE_POST, Group::TRADE_PUT])]
    #[NotBlank(message: "La valeur 'win' ne peut pas être vide.", groups: [ValidationGroup::POST_TRADE])]
    private ?float $win;

    #[Column(type: 'float')]
    #[Groups([Group::TRADE_GET, Group::TRADE_POST, Group::TRADE_PUT])]
    #[NotBlank(message: "La valeur 'loss' ne peut pas être vide.", groups: [ValidationGroup::POST_TRADE])]
    private ?float $loss;

    #[Column(type: 'boolean', nullable: true)]
    #[Groups([Group::TRADE_GET, Group::TRADE_POST, Group::TRADE_PUT])]
    private ?bool $passed;

    #[ManyToOne(targetEntity: Asset::class, inversedBy: 'trades')]
    #[Groups([Group::TRADE_GET, Group::TRADE_POST, Group::TRADE_PUT, Group::USER_GET])]
    #[NotBlank(message: "La valeur 'asset' ne peut pas être vide.", groups: [ValidationGroup::POST_TRADE])]
    private ?Asset $asset;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups([Group::TRADE_GET, Group::TRADE_POST, Group::TRADE_PUT])]
    #[Choice([self::STATUS_IN_PROGRESS, self::STATUS_LOSS, self::STATUS_NEUTRAL, self::STATUS_WIN])]
    private $status = self::STATUS_IN_PROGRESS;

    #[ORM\Column(type: 'float', nullable: true)]
    #[Groups([Group::TRADE_GET, Group::TRADE_POST, Group::TRADE_PUT])]
    private $walletBalance;

    #[ORM\Column(type: 'float', nullable: true)]
    #[Groups([Group::TRADE_GET, Group::TRADE_POST, Group::TRADE_PUT, Group::USER_GET])]
    private $progression;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups([Group::TRADE_GET, Group::TRADE_POST, Group::TRADE_PUT])]
    private $riskReward;

    #[ORM\ManyToOne(targetEntity: Category::class, inversedBy: 'trades')]
    #[Groups([Group::TRADE_GET, Group::TRADE_PUT, Group::USER_GET])]
    #[NotBlank(
        message: "La valeur 'category' ne peut pas être vide.",
        groups: [ValidationGroup::POST_TRADE, ValidationGroup::PUT_TRADE]
    )]
    private $category;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'trades')]
    #[Groups([Group::TRADE_GET, Group::TRADE_PUT])]
    #[NotBlank(message: "La valeur 'trader' ne peut pas être vide.", groups: [ValidationGroup::POST_TRADE])]
    private $trader;

    #[ORM\ManyToOne(targetEntity: Setup::class, inversedBy: 'trades')]
    #[Groups([Group::TRADE_GET, Group::TRADE_PUT, Group::USER_GET])]
    private $setup;

    #[ORM\ManyToOne(targetEntity: Platform::class, inversedBy: 'trades')]
    #[Groups([Group::TRADE_GET, Group::TRADE_POST, Group::TRADE_PUT, Group::USER_GET])]
    private $platform;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups([Group::TRADE_GET, Group::TRADE_PUT, Group::TRADE_POST, Group::USER_GET])]
    private $leverage;

    #[ORM\Column(type: 'boolean')]
    #[Groups([Group::TRADE_GET, Group::TRADE_PUT, Group::TRADE_POST])]
    private $favorite = false;

    #[ORM\OneToMany(mappedBy: 'trade', targetEntity: Notification::class, cascade: ['persist', 'remove'])]
    private $notifications;

    #[ORM\Column(type: 'string', length: 255, nullable: false)]
    #[Groups([Group::TRADE_GET, Group::TRADE_POST, Group::TRADE_PUT, Group::USER_GET])]
    private $pair = self::DEFAULT_PAIR;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups([Group::TRADE_GET, Group::TRADE_POST, Group::TRADE_PUT, Group::USER_GET])]
    private $comment;

    public function __construct()
    {
        $this->uuid = $this->generateUuid();
        $this->notifications = new ArrayCollection();
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }


    public function getEntry(): ?float
    {
        return $this->entry;
    }

    public function setEntry(?float $entry): self
    {
        $this->entry = $entry;

        return $this;
    }

    public function getStopLoss(): ?float
    {
        return $this->stopLoss;
    }

    public function setStopLoss(float $stopLoss): self
    {
        $this->stopLoss = $stopLoss;

        return $this;
    }

    public function getTakeProfit(): ?float
    {
        return $this->takeProfit;
    }

    public function setTakeProfit(float $takeProfit): self
    {
        $this->takeProfit = $takeProfit;

        return $this;
    }

    public function getNeededInput(): ?float
    {
        return $this->neededInput;
    }

    public function setNeededInput(float $neededInput): self
    {
        $this->neededInput = $neededInput;

        return $this;
    }

    public function getTimeframe(): ?string
    {
        return $this->timeframe;
    }

    public function setTimeframe(string $timeframe): self
    {
        $this->timeframe = $timeframe;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getWin(): ?float
    {
        return $this->win;
    }

    public function setWin(float $win): self
    {
        $this->win = $win;

        return $this;
    }

    public function getLoss(): ?float
    {
        return $this->loss;
    }

    public function setLoss(float $loss): self
    {
        $this->loss = $loss;

        return $this;
    }

    public function getPassed(): ?bool
    {
        return $this->passed;
    }

    public function setPassed(?bool $passed): self
    {
        $this->passed = $passed;

        return $this;
    }

    public function getAsset(): ?Asset
    {
        return $this->asset;
    }

    public function setAsset(?Asset $asset): self
    {
        $this->asset = $asset;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getWalletBalance(): ?float
    {
        return $this->walletBalance;
    }

    public function setWalletBalance(?float $walletBalance): self
    {
        $this->walletBalance = $walletBalance;

        return $this;
    }

    public function getProgression(): ?float
    {
        return $this->progression;
    }

    public function setProgression(?float $progression): self
    {
        $this->progression = $progression;

        return $this;
    }

    public function getRiskReward(): ?string
    {
        return $this->riskReward;
    }

    public function setRiskReward(?string $riskReward): self
    {
        $this->riskReward = $riskReward;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getTrader(): ?User
    {
        return $this->trader;
    }

    public function setTrader(?User $trader): self
    {
        $this->trader = $trader;

        return $this;
    }

    public function getSetup(): ?Setup
    {
        return $this->setup;
    }

    public function setSetup(?Setup $setup): self
    {
        $this->setup = $setup;

        return $this;
    }

    public function getPlatform(): ?Platform
    {
        return $this->platform;
    }

    public function setPlatform(?Platform $platform): self
    {
        $this->platform = $platform;

        return $this;
    }

    public function isWon(): bool
    {
        return $this->getStatus() === self::STATUS_WIN;
    }
    public function isLost(): bool
    {
        return $this->getStatus() === self::STATUS_LOSS;
    }
    public function isInProgress(): bool
    {
        return $this->getStatus() === self::STATUS_IN_PROGRESS;
    }
    public function isNeutral(): bool
    {
        return $this->getStatus() === self::STATUS_NEUTRAL;
    }
    public function isOver(): bool
    {
        return !$this->isInProgress();
    }

    public function getLeverage(): ?int
    {
        return $this->leverage;
    }

    public function setLeverage(?int $leverage): self
    {
        $this->leverage = $leverage;

        return $this;
    }

    public function getFavorite(): ?bool
    {
        return $this->favorite;
    }

    public function setFavorite(bool $favorite): self
    {
        $this->favorite = $favorite;

        return $this;
    }

    /**
     * @return Collection<int, Notification>
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications[] = $notification;
            $notification->setTrade($this);
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->removeElement($notification)) {
            // set the owning side to null (unless already changed)
            if ($notification->getTrade() === $this) {
                $notification->setTrade(null);
            }
        }

        return $this;
    }

    public function getPair(): ?string
    {
        return $this->pair;
    }

    public function setPair(?string $pair): self
    {
        $this->pair = $pair;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function isLong(): bool
    {
        return $this->type === self::TYPE_LONG;
    }
    public function isShort(): bool
    {
        return $this->type === self::TYPE_SHORT;
    }
}
