<?php

namespace App\Entity\Traits;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Generic\Group;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Annotation\Groups;

trait IdentifierTraits
{
    #[Id]
    #[ApiProperty(identifier: false)]
    #[GeneratedValue(strategy: 'IDENTITY')]
    #[Column(type: 'integer')]
    #[Groups([
        Group::SETUP_GET, Group::TRADE_GET, Group::PLATFORM_GET, Group::ASSET_GET, Group::RULES_GET,
        Group::USER_GET, Group::CATEGORY_GET, Group::WALLET_GET, Group::NOTIFICATION_GET, Group::PARAMETER_GET
    ])]
    private ?int $id;

    #[Column(type: 'uuid', unique: true)]
    #[ApiProperty(identifier: true)]
    #[Groups([
        Group::SETUP_GET, Group::TRADE_GET, Group::PLATFORM_GET, Group::ASSET_GET, Group::RULES_GET,
        Group::USER_GET, Group::CATEGORY_GET, Group::WALLET_GET, Group::NOTIFICATION_GET, Group::PARAMETER_GET
    ])]
    private $uuid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function generateUuid(): UuidInterface
    {
        return Uuid::uuid4();
    }
}
