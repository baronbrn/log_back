<?php

namespace App\Entity\Traits;

use App\Generic\Group;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\PrePersist;
use Symfony\Component\Serializer\Annotation\Groups;

trait TimestampTraits
{
    #[Column(type: 'datetime')]
    #[Groups([Group::USER_GET, Group::TRADE_GET, Group::SETUP_GET, Group::NOTIFICATION_GET])]
    private $createdAt;


    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    #[PrePersist]
    public function setCreatedAt(): void
    {
        $this->createdAt = new \DateTime();
    }
}
