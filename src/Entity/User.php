<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\MeController;
use App\Entity\Traits\IdentifierTraits;
use App\Entity\Traits\TimestampTraits;
use App\Generic\Group;
use App\Generic\Role;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints\Choice;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[ApiResource(
    collectionOperations: [
        "get" => [
            "security" => "is_granted('ROLE_ADMIN')"
        ],
        "post" => [],
        "me" => [
            'pagination_enabled' => false,
            'path' => '/me',
            'method' => 'GET',
            'controller' => MeController::class,
            'read' => false,
            'openapi_context' => [
                'security' => [
                    ['bearerAuth' => []]
                ]
            ]
        ]
    ],
    itemOperations: [
        "get" => [
            "security" => "is_granted('GET_USER', object)"
        ],
        "put" => [
            "security" => "is_granted('PUT_USER', object)"
        ],
        "delete" => [
            "security" => "is_granted('ROLE_ADMIN')"
        ]
    ],
    denormalizationContext: ['groups' => [Group::USER_POST, Group::USER_PUT]],
    normalizationContext: ['groups' => [Group::USER_GET]],
)]
#[HasLifecycleCallbacks]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    use IdentifierTraits;
    use TimestampTraits;

    public const GENDER_MALE = 'male';
    public const GENDER_FEMALE = 'female';
    public const GENDER_OTHER = 'other';

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    #[Groups([Group::USER_GET, Group::USER_POST, Group::USER_PUT])]
    private ?string $email;

    #[ORM\Column(type: 'json')]
    #[Groups([Group::USER_GET, Group::USER_POST, Group::USER_PUT])]
    private array $roles = [];

    #[ORM\Column(type: 'string')]
    #[Groups([Group::USER_POST, Group::USER_PUT])]
    private string $password;

    #[ORM\OneToMany(mappedBy: 'trader', targetEntity: Trade::class)]
    #[Groups([Group::USER_GET])]
    private $trades;

    #[ORM\OneToMany(mappedBy: 'trader', targetEntity: Setup::class, cascade: ['persist', 'remove'])]
    private $setups;

    #[ORM\OneToMany(mappedBy: 'trader', targetEntity: Wallet::class)]
    #[Groups([Group::USER_GET])]
    private $wallets;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups([Group::USER_GET, Group::USER_POST, Group::USER_PUT])]
    private $username;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups([Group::USER_GET, Group::USER_POST, Group::USER_PUT])]
    private $birthdate;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups([Group::USER_GET, Group::USER_POST, Group::USER_PUT])]
    private $firstName;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups([Group::USER_GET, Group::USER_POST, Group::USER_PUT])]
    private $lastName;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups([Group::USER_GET, Group::USER_POST, Group::USER_PUT])]
    private $phone;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups([Group::USER_GET, Group::USER_POST, Group::USER_PUT])]
    private $country;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups([Group::USER_GET, Group::USER_POST, Group::USER_PUT])]
    #[Choice([self::GENDER_MALE, self::GENDER_FEMALE, self::GENDER_OTHER])]
    private $gender;

    #[ORM\OneToMany(mappedBy: 'trader', targetEntity: Notification::class)]
    #[Groups([Group::USER_GET, Group::USER_PUT])]
    private $notifications;

    #[ORM\OneToOne(inversedBy: 'trader', targetEntity: Parameter::class, cascade: ['persist', 'remove'])]
    #[Groups([Group::USER_GET])]
    private $parameter;

    public function __construct()
    {
        $this->uuid = $this->generateUuid();
        $this->trades = new ArrayCollection();
        $this->setups = new ArrayCollection();
        $this->wallets = new ArrayCollection();
        $this->notifications = new ArrayCollection();
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = Role::USER;

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection<int, Trade>
     */
    public function getTrades(): Collection
    {
        return $this->trades;
    }

    public function addTrade(Trade $trade): self
    {
        if (!$this->trades->contains($trade)) {
            $this->trades[] = $trade;
            $trade->setTrader($this);
        }

        return $this;
    }

    public function removeTrade(Trade $trade): self
    {
        if ($this->trades->removeElement($trade)) {
            // set the owning side to null (unless already changed)
            if ($trade->getTrader() === $this) {
                $trade->setTrader(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Setup>
     */
    public function getSetups(): Collection
    {
        return $this->setups;
    }

    public function addSetup(Setup $setup): self
    {
        if (!$this->setups->contains($setup)) {
            $this->setups[] = $setup;
            $setup->setTrader($this);
        }

        return $this;
    }

    public function removeSetup(Setup $setup): self
    {
        if ($this->setups->removeElement($setup)) {
            // set the owning side to null (unless already changed)
            if ($setup->getTrader() === $this) {
                $setup->setTrader(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Wallet>
     */
    public function getWallets(): Collection
    {
        return $this->wallets;
    }

    public function addWallet(Wallet $wallet): self
    {
        if (!$this->wallets->contains($wallet)) {
            $this->wallets[] = $wallet;
            $wallet->setTrader($this);
        }

        return $this;
    }

    public function removeWallet(Wallet $wallet): self
    {
        if ($this->wallets->removeElement($wallet)) {
            // set the owning side to null (unless already changed)
            if ($wallet->getTrader() === $this) {
                $wallet->setTrader(null);
            }
        }

        return $this;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(?\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return float|int|null
     */
    #[SerializedName('balance')]
    #[Groups([Group::USER_GET])]
    public function getUserTotalBalance(): float|int|null
    {
        if ($this->getBalanceSpot() && $this->getBalanceFuture()) {
            return $this->getBalanceSpot() + $this->getBalanceFuture();
        }
        return null;
    }
    #[SerializedName('balanceSpot')]
    #[Groups([Group::USER_GET])]
    public function getBalanceSpot()
    {
        $wallet = $this->wallets->filter(function (Wallet $wallet) {
            return $wallet->getCategory()->getName() === Category::SPOT;
        });
        return $wallet->first() ? $wallet->first()->getBalance() : null;
    }
    #[SerializedName('balanceFuture')]
    #[Groups([Group::USER_GET])]
    public function getBalanceFuture()
    {
        $wallet = $this->wallets->filter(function (Wallet $wallet) {
            return $wallet->getCategory()->getName() === Category::FUTURE;
        });
        return $wallet->first() ? $wallet->first()->getBalance() : null;
    }
    //public function getBalancePaper(){}

    #[SerializedName('tradeWonSpot')]
    #[Groups([Group::USER_GET])]
    public function getTradeWonSpot(): int
    {
        return count($this->filterTrade(Trade::STATUS_WIN, Category::SPOT));
    }
    #[SerializedName('tradeWonFuture')]
    #[Groups([Group::USER_GET])]
    public function getTradeWonFuture(): int
    {
        return count($this->filterTrade(Trade::STATUS_WIN, Category::FUTURE));
    }

    #[SerializedName('tradeLostSpot')]
    #[Groups([Group::USER_GET])]
    public function getTradeLostSpot(): int
    {
        return count($this->filterTrade(Trade::STATUS_LOSS, Category::SPOT));
    }
    #[SerializedName('tradeLostFuture')]
    #[Groups([Group::USER_GET])]
    public function getTradeLostFuture(): int
    {
        return count($this->filterTrade(Trade::STATUS_LOSS, Category::FUTURE));
    }

    #[SerializedName('tradeInProgressSpot')]
    #[Groups([Group::USER_GET])]
    public function getTradeInProgressSpot(): int
    {
        return count($this->filterTrade(Trade::STATUS_IN_PROGRESS, Category::SPOT));
    }
    #[SerializedName('tradeInProgressFuture')]
    #[Groups([Group::USER_GET])]
    public function getTradeInProgressFuture(): int
    {
        return count($this->filterTrade(Trade::STATUS_IN_PROGRESS, Category::FUTURE));
    }

    #[SerializedName('tradeNeutralSpot')]
    #[Groups([Group::USER_GET])]
    public function getTradeIsNeutralSpot(): int
    {
        return count($this->filterTrade(Trade::STATUS_NEUTRAL, Category::SPOT));
    }
    #[SerializedName('tradeNeutralFuture')]
    #[Groups([Group::USER_GET])]
    public function getTradeIsNeutralFuture(): int
    {
        return count($this->filterTrade(Trade::STATUS_NEUTRAL, Category::FUTURE));
    }

    public function filterTrade(string $status, ?string $category = null): Collection
    {
        return $this->getTrades()->filter(function (Trade $trade) use ($status, $category) {
            if ($category) {
                return $trade->getStatus() === $status && $trade->getCategory()->getName() === $category;
            }
            return $trade->getStatus() === $status;
        });
    }

    #[SerializedName('tradeInProgress')]
    #[Groups([Group::USER_GET])]
    public function getInProgressTrade(): Collection
    {
        return $this->filterTrade(Trade::STATUS_IN_PROGRESS);
    }

    /**
     * @return Collection<int, Notification>
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications[] = $notification;
            $notification->setTrader($this);
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->removeElement($notification)) {
            // set the owning side to null (unless already changed)
            if ($notification->getTrader() === $this) {
                $notification->setTrader(null);
            }
        }

        return $this;
    }

    public function getParameter(): ?Parameter
    {
        return $this->parameter;
    }

    public function setParameter(?Parameter $parameter): self
    {
        $this->parameter = $parameter;

        return $this;
    }

    public function isAdmin(): bool
    {
        return in_array(Role::ADMIN, $this->getRoles());
    }
}
