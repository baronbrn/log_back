<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\IdentifierTraits;
use App\Generic\Group;
use App\Repository\WalletRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: WalletRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get" => [
            "security" => "is_granted('ROLE_TRADER')"
        ]
    ],
    itemOperations: [
        "get" => [
            "security" => "is_granted('GET_WALLET', object)"
        ],
        "put" => [
            "security" => "is_granted('ROLE_ADMIN')"
        ],
        "delete" => [
            "security" => "is_granted('ROLE_ADMIN')"
        ]
    ],
    denormalizationContext: ['groups' => [Group::WALLET_POST, Group::WALLET_PUT]],
    normalizationContext: ['groups' => [Group::WALLET_GET]]
)]
class Wallet
{
    use IdentifierTraits;

    #[ORM\Column(type: 'float', nullable: true)]
    #[Groups([Group::WALLET_GET, Group::WALLET_PUT, Group::WALLET_POST, Group::USER_GET])]
    private $balance = 0.0;

    #[ORM\ManyToOne(targetEntity: Category::class, inversedBy: 'wallets')]
    #[Groups([Group::WALLET_GET, Group::USER_GET])]
    private $category;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'wallets')]
    private $trader;

    public function __construct()
    {
        $this->uuid = $this->generateUuid();
    }

    public function getBalance(): ?float
    {
        return $this->balance;
    }

    public function setBalance(?float $balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getTrader(): ?User
    {
        return $this->trader;
    }

    public function setTrader(?User $trader): self
    {
        $this->trader = $trader;

        return $this;
    }
}
