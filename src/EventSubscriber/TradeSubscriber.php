<?php

namespace App\EventSubscriber;

use App\Entity\Notification;
use App\Entity\Trade;
use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class TradeSubscriber implements EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::preUpdate
        ];
    }

    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        if (!$eventArgs->getObject() instanceof Trade) {
            return;
        }

        /** @var Trade $trade */
        $trade = $eventArgs->getObject();

        $this->onTradeOver($trade, true);
    }

    public function preUpdate(LifecycleEventArgs $eventArgs)
    {
        if (!$eventArgs->getObject() instanceof Trade) {
            return;
        }

        /** @var Trade $trade */
        $trade = $eventArgs->getObject();

        $this->onTradeOver($trade, false);
    }

    public function onTradeOver(Trade $trade, bool $read): Trade
    {
        if ($trade->isOver()) {
            $trade->addNotification(
                (new Notification())
                    ->setContent($this->getNotificationContent($trade))
                    ->setTrader($trade->getTrader())
                    ->setTrade($trade)
                    ->setRead($read)
            );
        }
        return $trade;
    }

    public function getNotificationContent(Trade $trade): string
    {
        $content = '';
        $asset = $trade->getAsset()->getName();
        switch ($trade) {
            case $trade->isWon():
                $content = 'Trade ' . $asset . ' passed!';
                break;
            case $trade->isLost():
                $content = 'Trade ' . $asset . ' lost';
                break;
            case $trade->isNeutral():
                $content = 'Trade ' . $asset . ' break even';
                break;
        }
        return $content;
    }
}
