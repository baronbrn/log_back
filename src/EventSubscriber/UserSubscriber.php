<?php

namespace App\EventSubscriber;

use App\Entity\Currency;
use App\Entity\Parameter;
use App\Entity\User;
use App\Entity\Wallet;
use App\Repository\CategoryRepository;
use App\Repository\CurrencyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class UserSubscriber implements EventSubscriber
{
    private CurrencyRepository $currencyRepository;
    private CategoryRepository $categoryRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        CurrencyRepository $currencyRepository
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->currencyRepository = $currencyRepository;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist
        ];
    }

    public function postPersist(LifecycleEventArgs $eventArgs)
    {
        if (!$eventArgs->getObject() instanceof User) {
            return;
        }

        /** @var User $user */
        $user = $eventArgs->getObject();

        /** @var EntityManagerInterface $em */
        $em = $eventArgs->getEntityManager();

        $categories = $this->categoryRepository->findAll();

        foreach ($categories as $category) {
            $wallet = (new Wallet())
                ->setBalance(0)
                ->setCategory($category)
                ->setTrader($user);
            $em->persist($wallet);
            $user->addWallet($wallet);
        }
        $user->setParameter(
            (new Parameter())
                ->setCurrency($this->currencyRepository->findOneBy(['name' => 'Dollar']))
                ->setLanguage('English')
        );

        $em->persist($user);
        $em->flush();
    }
}
