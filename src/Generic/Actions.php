<?php

namespace App\Generic;

use ReflectionClass;

class Actions
{
    public const GET = 'GET';
    public const POST = 'POST';
    public const PUT = 'PUT';
    public const DELETE = 'DELETE';

    public static function getConstants(): array
    {
        $reflexionClass = new ReflectionClass(static::class);
        return $reflexionClass->getConstants();
    }
}
