<?php

namespace App\Generic;

class Group
{
    public const ASSET_GET = 'asset:get';
    public const ASSET_POST = 'asset:post';
    public const ASSET_PUT = 'asset:put';

    public const PLATFORM_GET = 'platform:get';
    public const PLATFORM_POST = 'platform:post';

    public const SETUP_GET = 'setup:get';
    public const SETUP_POST = 'setup:post';
    public const SETUP_PUT = 'setup:put';

    public const TRADE_GET = 'trade:get';
    public const TRADE_POST = 'trade:post';
    public const TRADE_PUT = 'trade:put';

    public const USER_GET = 'user:get';
    public const USER_PUT = 'user:put';
    public const USER_POST = 'user:post';

    public const RULES_GET = 'rules:get';
    public const RULES_POST = 'rules:post';
    public const RULES_PUT = 'rules:put';

    public const CATEGORY_GET = 'category:get';
    public const CATEGORY_POST = 'category:post';
    public const CATEGORY_PUT = 'category:put';

    public const WALLET_GET = 'wallet:get';
    public const WALLET_POST = 'wallet:post';
    public const WALLET_PUT = 'wallet:put';

    public const NOTIFICATION_GET = 'notification:get';
    public const NOTIFICATION_POST = 'notification:post';
    public const NOTIFICATION_PUT = 'notification:put';

    public const PARAMETER_GET = 'parameter:get';
    public const PARAMETER_PUT = 'parameter:put';

    public const CURRENCY_GET = 'currency:get';
    public const CURRENCY_POST = 'currency:post';
}
