<?php

namespace App\Generic;

class Role
{
    public const USER = 'ROLE_USER';
    public const TRADER = 'ROLE_TRADER';
    public const ADMIN = 'ROLE_ADMIN';
}
