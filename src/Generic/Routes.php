<?php

namespace App\Generic;

class Routes
{
    public const TRADE = '/trades';
    public const USER = '/users';
    public const CATEGORY = '/categories';
    public const RULES = '/rules';
    public const ASSET = '/assets';
    public const WALLET = '/wallets';
    public const NOTIFICATION = '/notifications';
    public const SETUP = '/setups';
    public const PARAMETER = '/parameters';
    public const CURRENCY = '/currencies';
}
