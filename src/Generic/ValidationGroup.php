<?php

namespace App\Generic;

class ValidationGroup
{
    public const DEFAULT = 'Default';

    public const POST_TRADE = 'trade:post:validation';
    public const PUT_TRADE = 'trade:put:validation';

    public const PUT_PARAMETER = 'parameter:put:validation';
}
