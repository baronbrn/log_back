<?php

namespace App\Repository;

use App\Entity\Trade;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Trade|null find($id, $lockMode = null, $lockVersion = null)
 * @method Trade|null findOneBy(array $criteria, array $orderBy = null)
 * @method Trade[]    findAll()
 * @method Trade[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TradeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Trade::class);
    }

    public function findByTrader(User $user)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.trader = :user')
            ->setParameter('user', $user)
            ->getQuery()->getResult();
    }

    public function findByUserAndType(User $user, string $type)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.trader = :user')
            ->andWhere('t.type = :type')
            ->setParameter('user', $user)
            ->setParameter('type', $type)
            ->getQuery()->getResult();
    }

    public function findByUserAndStatus(User $user, string $status)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.trader = :user')
            ->andWhere('t.status = :status')
            ->setParameter('user', $user)
            ->setParameter('status', $status)
            ->getQuery()->getResult();
    }

    public function findByUserAndCategory(User $user, string $categoryName)
    {
        return $this->createQueryBuilder('t')
            ->innerJoin('t.category', 'tc')
            ->andWhere('tc.name = :name')
            ->andWhere('t.trader = :user')
            ->setParameter('name', $categoryName)
            ->setParameter('user', $user)
            ->getQuery()->getResult();
    }

    public function findByUserAndAsset(User $user, string $assetName)
    {
        return $this->createQueryBuilder('t')
            ->innerJoin('t.asset', 'ta')
            ->andWhere('ta.name = :name')
            ->andWhere('t.trader = :user')
            ->setParameter('name', $assetName)
            ->setParameter('user', $user)
            ->getQuery()->getResult();
    }

    public function findByUserAndCategoryAndStatus(User $user, string $category, string $status)
    {
        return $this->createQueryBuilder('t')
            ->innerJoin('t.category', 'tc')
            ->andWhere('tc.name = :name')
            ->andWhere('t.status = :status')
            ->andWhere('t.trader = :user')
            ->setParameter('status', $status)
            ->setParameter('name', $category)
            ->setParameter('user', $user)
            ->getQuery()->getResult();
    }

    // /**
    //  * @return Trade[] Returns an array of Trade objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Trade
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
