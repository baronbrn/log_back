<?php

namespace App\Security\Voter;

use App\Entity\User;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

abstract class AbstractVoter extends Voter
{
    protected $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function arrayOf($subject, $entityType): bool
    {
        return is_array($subject) && (is_a($subject[0], $entityType));
    }

    public function isOwner(User $trader, User $securityUser): bool
    {
        return $trader->getId() == $securityUser->getId();
    }

    public function isIdentified($user): bool
    {
        return $user instanceof User;
    }
}
