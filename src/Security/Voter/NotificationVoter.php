<?php

namespace App\Security\Voter;

use App\Entity\Notification;
use App\Entity\Rules;
use App\Entity\User;
use App\Generic\Role;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class NotificationVoter extends AbstractVoter
{
    public const GET = 'GET_NOTIFICATION';
    public const PUT = 'PUT_NOTIFICATION';
    public const DELETE = 'DELETE_NOTIFICATION';

    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, [self::GET, self::PUT, self::DELETE])
            && ($subject instanceof Notification || $this->arrayOf($subject, Notification::class));
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        /** @var Notification $subject */

        /** @var User $user */
        $user = $token->getUser();

        if (!$this->isIdentified($user)) {
            return false;
        }
        if ($this->security->isGranted(Role::ADMIN)) {
            return true;
        }

        switch ($attribute) {
            case self::GET || self::PUT || self::DELETE:
                return $this->isOwner($user, $subject->getTrader());
        }
        return false;
    }
}
