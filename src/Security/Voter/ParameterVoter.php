<?php

namespace App\Security\Voter;

use App\Entity\Notification;
use App\Entity\Parameter;
use App\Entity\User;
use App\Generic\Role;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class ParameterVoter extends AbstractVoter
{
    public const GET = 'GET_PARAMETER';
    public const PUT = 'PUT_PARAMETER';

    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, [self::GET, self::PUT]) && ($subject instanceof Parameter);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        /** @var Parameter $subject */

        /** @var User $user */
        $user = $token->getUser();

        if (!$this->isIdentified($user)) {
            return false;
        }
        if ($this->security->isGranted(Role::ADMIN)) {
            return true;
        }

        switch ($attribute) {
            case self::GET || self::PUT:
                return $this->isOwner($user, $subject->getTrader());
        }
        return false;
    }
}
