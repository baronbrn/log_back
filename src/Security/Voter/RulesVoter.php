<?php

namespace App\Security\Voter;

use App\Entity\Rules;
use App\Entity\User;
use App\Generic\Role;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class RulesVoter extends AbstractVoter
{
    public const GET = 'GET_RULES';
    public const PUT = 'PUT_RULES';
    public const DELETE = 'DELETE_RULES';

    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, [self::GET, self::PUT, self::DELETE])
            && ($subject instanceof Rules || $this->arrayOf($subject, Rules::class));
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        /** @var Rules $subject */

        /** @var User $user */
        $user = $token->getUser();

        if (!$this->isIdentified($user)) {
            return false;
        }
        if ($this->security->isGranted(Role::ADMIN)) {
            return true;
        }

        switch ($attribute) {
            case self::GET || self::PUT || self::DELETE:
                return $user->getSetups()->contains($subject->getSetup());
        }
        return false;
    }
}
