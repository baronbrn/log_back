<?php

namespace App\Security\Voter;

use App\Entity\Setup;
use App\Entity\User;
use App\Generic\Role;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class SetupVoter extends AbstractVoter
{
    public const GET = 'GET_SETUP';
    public const PUT = 'PUT_SETUP';
    public const DELETE = 'DELETE_SETUP';

    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, [self::GET, self::PUT, self::DELETE])
            && ($subject instanceof Setup || $this->arrayOf($subject, Setup::class));
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        /** @var Setup $subject */

        /** @var User $user */
        $user = $token->getUser();

        if (!$this->isIdentified($user)) {
            return false;
        }
        if ($this->security->isGranted(Role::ADMIN)) {
            return true;
        }

        $trader = $subject->getTrader();

        switch ($attribute) {
            case self::GET || self::PUT || self::DELETE:
                return $this->isOwner($trader, $user);
        }
        return false;
    }
}
