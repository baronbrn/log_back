<?php

namespace App\Security\Voter;

use App\Entity\Trade;
use App\Entity\User;
use App\Generic\Role;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class TradeVoter extends AbstractVoter
{
    public const GET = 'GET_TRADE';
    public const PUT = 'PUT_TRADE';
    public const DELETE = 'DELETE_TRADE';

    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, [self::GET, self::PUT, self::DELETE])
            && ($subject instanceof Trade || $this->arrayOf($subject, Trade::class));
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        /** @var Trade $subject */

        /** @var User $user */
        $user = $token->getUser();

        if (!$this->isIdentified($user)) {
            return false;
        }
        if ($this->security->isGranted(Role::ADMIN)) {
            return true;
        }

        $trader = $subject->getTrader();

        switch ($attribute) {
            case self::GET || self::PUT || self::DELETE:
                return $this->isOwner($trader, $user);
        }
        return false;
    }
}
