<?php

namespace App\Security\Voter;

use App\Entity\Trade;
use App\Entity\User;
use App\Generic\Role;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class UserVoter extends AbstractVoter
{
    public const GET = 'GET_USER';
    public const PUT = 'PUT_USER';
    public const DELETE = 'DELETE_USER';

    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, [self::GET, self::PUT, self::DELETE])
            && ($subject instanceof User || $this->arrayOf($subject, User::class));
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        /** @var User $subject */

        /** @var User $user */
        $user = $token->getUser();

        if (!$this->isIdentified($user)) {
            return false;
        }
        if ($this->security->isGranted(Role::ADMIN)) {
            return true;
        }

        switch ($attribute) {
            case self::GET || self::PUT:
                return $this->isOwner($subject, $user);
        }
        return false;
    }
}
