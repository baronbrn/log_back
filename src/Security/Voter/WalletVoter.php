<?php

namespace App\Security\Voter;

use App\Entity\User;
use App\Entity\Wallet;
use App\Generic\Role;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class WalletVoter extends AbstractVoter
{
    public const GET = 'GET_WALLET';

    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, [self::GET])
            && ($subject instanceof Wallet || $this->arrayOf($subject, Wallet::class));
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        /** @var Wallet $subject */

        /** @var User $user */

        $user = $token->getUser();

        if (!$this->isIdentified($user)) {
            return false;
        }
        if ($this->security->isGranted(Role::ADMIN)) {
            return true;
        }

        switch ($attribute) {
            case self::GET:
                return $this->isOwner($subject->getTrader(), $user);
        }
        return false;
    }
}
