<?php

namespace App\Utils;

use DateTime;
use DateTimeInterface;

class Utils
{
    public static function dateToString(DateTimeInterface $date, string $format = 'Y-m-d H:i:s'): string
    {
        return $date->format($format);
    }

    public static function updateTime(DateTime $date, int $hours, int $minute): DateTime|\Exception
    {
        try {
            return (new DateTime(self::dateToString($date)))->setTime($hours, $minute);
        } catch (\Exception $e) {
            return $e;
        }
    }
}
