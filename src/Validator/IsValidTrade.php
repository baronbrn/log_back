<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[\Attribute] class IsValidTrade extends Constraint
{
    public string $messageLeverage = "Le champ 'leverage' est obligatoire si vous choisissez la catégorie future.";

    public function getTargets(): array|string
    {
        return self::CLASS_CONSTRAINT;
    }
}
