<?php

namespace App\Validator;

use App\Entity\Trade;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsValidTradeValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint IsValidTrade */

        if (null === $value || '' === $value) {
            return;
        }

        /* @var Trade $value */

        if (
            ($value->getCategory() && $value->getCategory()->isFuture()) &&
            is_null($value->getLeverage())
        ) {
            $this->context->buildViolation($constraint->messageLeverage)->addViolation();
        }
    }
}
