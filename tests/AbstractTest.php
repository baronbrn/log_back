<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use App\Entity\User;
use App\Generic\Actions;
use App\Tests\Utils\Credentials;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractTest extends ApiTestCase
{
    private $token;
    private $clientWithCredentials;

    public function setUp(): void
    {
        self::bootKernel();
    }

    public function buildRequest(string $credential, string $action, string $route, int $expected, ?array $body=null)
    {
        $res = ($action === Actions::POST || $action === Actions::PUT)?
            $this->createClientWithCredentials($credential)->request($action, $route, ['json'=>$body]) : $this->createClientWithCredentials($credential)->request($action, $route);
        self::assertResponseStatusCodeSame($expected);
        return self::decode($res);
    }

    protected function createClientWithAdminCredentials(): Client
    {
        $token = $this->getToken(['username' => 'admin@email.com', 'password' => 'password']);

        return $this->getClient($token);
    }

    protected function createClientWithUserCredentials(): Client
    {
        $token = $this->getToken();

        return $this->getClient($token);
    }

    /**
     * @throws \JsonException
     */
    protected function createClientWithCredentials(string $jsonCredentials): Client|null
    {
        $token = $this->getToken(json_decode($jsonCredentials, true, 512, JSON_THROW_ON_ERROR));

        return $this->getClient($token);
    }

    protected function getClient(string $token): Client|null
    {
        return static::createClient([], [
            'headers' => [
                'authorization' => 'Bearer '.$token,
                'Content-Type' => 'application/json'
            ]
        ]);
    }

    protected function getToken($body = []): string
    {
        if ($this->token) {
            return $this->token;
        }

        $body = $body ?: [
            'username' => 'user@email.com',
            'password' => 'password',
        ];

        $response = static::createClient([], ['headers' => ['Content-Type' => 'application/json']])->request('POST', '/login', ['body' => json_encode($body)]);

        self::assertResponseIsSuccessful();
        $data = json_decode($response->getContent());
        $this->token = $data->token;

        return $data->token;
    }

    public static function dateToString(DateTime $date, string $format = 'Y-m-d H:i:s'): string
    {
        return $date->format($format);
    }

    public static function decode($result)
    {
        return json_decode($result->getContent(false), true);
    }

    public static function getRepository(string $class)
    {
        return static::getContainer()->get('doctrine')->getRepository($class);
    }

    public function getEntityManager()
    {
        return static::getContainer()->get(EntityManagerInterface::class);
    }

    public static function getUser(?string $email=null): User
    {
        $email = ($email) ?: json_decode(Credentials::TRADER)->username;

        return self::getRepository(User::class)->findByEmail($email);
    }

    public function getTraderCredential()
    {
        return Credentials::buildCredential('trader0@gmail.com');
    }

    public function getTradersCredential(): \Generator
    {
        for ($i=0;$i<6;$i++) {
            yield [Credentials::buildCredential('trader'.$i.'@gmail.com')];
        }
    }
}
