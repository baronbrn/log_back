<?php

namespace App\Tests\Api;

use App\Entity\Asset;
use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use App\Tests\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class AssetTest extends AbstractTest
{
    public function testGetAsset() {
        $this->buildRequest(
            Credentials::TRADER, Actions::GET, Routes::ASSET, Response::HTTP_OK
        );
    }
    public function testGetAssetItem() {
        $this->buildRequest(
            Credentials::TRADER, Actions::GET, Routes::ASSET . '/' . Utils::getAssets()[0]->getUuid(), Response::HTTP_OK
        );
    }
    public function testPostAsset() {
        $asset = $this->buildRequest(
            Credentials::ADMIN, Actions::POST, Routes::ASSET, Response::HTTP_CREATED,
            [ 'name' => $name = 'someName', 'symbol' => $symbol = 'SM']
        );
        self::assertSame($asset['name'], $name);
        self::assertSame($asset['symbol'], $symbol);
    }
    public function testPutAsset() {
        $asset = $this->buildRequest(
            Credentials::ADMIN, Actions::PUT, Routes::ASSET . '/' . Utils::getAssets()[0]->getUuid(), Response::HTTP_OK,
            [ 'name' => $updatedName = 'new name' ]
        );
        self::assertSame($asset['name'], $updatedName);
    }
}
