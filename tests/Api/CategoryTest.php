<?php

namespace App\Tests\Api;

use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use App\Tests\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class CategoryTest extends AbstractTest
{
    public function testGetCategory() {
        $this->buildRequest(
            Credentials::TRADER, Actions::GET, Routes::CATEGORY, Response::HTTP_OK
        );
    }
    public function testGetCategoryItem() {
        $this->buildRequest(
            Credentials::TRADER, Actions::GET, Routes::CATEGORY . '/' . Utils::getCategories()[0]->getUuid(), Response::HTTP_OK
        );
    }
    public function testPostCategory() {
        $category = $this->buildRequest(
            Credentials::ADMIN, Actions::POST, Routes::CATEGORY, Response::HTTP_CREATED,
            [ 'name' => $name = 'someName' ]
        );
        self::assertSame($category['name'], $name);
    }
    public function testPutCategory() {
        $category = $this->buildRequest(
            Credentials::ADMIN, Actions::POST, Routes::CATEGORY, Response::HTTP_CREATED,
            [ 'name' => 'paper' ]
        );

        $category = $this->buildRequest(
            Credentials::ADMIN, Actions::PUT, Routes::CATEGORY.'/'.$category['uuid'], Response::HTTP_OK,
            [ 'name' => $updatedName = 'new name' ]
        );
        self::assertSame($category['name'], $updatedName);
    }
}
