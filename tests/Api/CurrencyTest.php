<?php

namespace App\Tests\Api;

use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use App\Tests\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class CurrencyTest extends AbstractTest
{
    public function testGetCurrencyList() {
        $this->buildRequest(
            Credentials::TRADER, Actions::GET, Routes::CURRENCY, Response::HTTP_OK
        );
    }
    public function testGetCurrencyItem() {
        $this->buildRequest(
            Credentials::TRADER, Actions::GET, Routes::CURRENCY . '/' . Utils::getCurrencies()[0]->getUuid(), Response::HTTP_OK
        );
    }
    public function testPostCurrency() {
        $currency = $this->buildRequest(
            Credentials::ADMIN, Actions::POST, Routes::ASSET, Response::HTTP_CREATED,
            [ 'name' => $name = 'rouble' ]);
        self::assertSame($currency['name'], $name);
    }
    public function testPutCurrency() {
        $this->buildRequest(
            Credentials::ADMIN, Actions::PUT, Routes::CURRENCY . '/' . Utils::getCurrencies()[0]->getUuid(), Response::HTTP_METHOD_NOT_ALLOWED, []
        );
    }
    public function testDeleteCurrency(): void {
        $this->buildRequest(
            Credentials::ADMIN, Actions::DELETE, Routes::CURRENCY . '/' . Utils::getCurrencies()[0]->getUuid(), Response::HTTP_METHOD_NOT_ALLOWED
        );
    }
}
