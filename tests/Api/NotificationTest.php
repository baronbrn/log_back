<?php

namespace App\Tests\Api;

use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use App\Tests\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class NotificationTest extends AbstractTest
{
    public function testGetNotification(): void {
        $this->buildRequest(Credentials::ADMIN, Actions::GET, Routes::NOTIFICATION, Response::HTTP_OK);
    }
    public function testGetNotificationItem(): void {
        $notification=Utils::getNotifications()[0];
        $this->buildRequest(Credentials::ADMIN, Actions::GET, Routes::NOTIFICATION.'/'.$notification->getUuid(), Response::HTTP_OK);
    }

    public function testPostNotification(): void {
        $trader=Utils::getDefaultTrader();
        $notification=$this->buildRequest(
            Credentials::ADMIN,
            Actions::POST,
            Routes::NOTIFICATION,
            Response::HTTP_CREATED, [
                'content'=>$content='someName',
                'read'=>false,
                'trader'=>'/users/'.$trader->getUuid(),
                'trade'=>'/trades/'.$trader->getTrades()->first()->getUuid()
        ]);
        self::assertSame($notification['content'], $content);
        self::assertSame($notification['read'], false);
    }

    public function testPutNotification(): void {
        $notification=Utils::getNotifications()[0];
        $this->buildRequest(Credentials::ADMIN, Actions::PUT, Routes::NOTIFICATION.'/'.$notification->getUuid(), Response::HTTP_OK, [
            'read'=>false
        ]);
    }
}
