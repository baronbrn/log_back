<?php

namespace App\Tests\Api;

use App\Entity\Parameter;
use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use App\Tests\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class ParameterTest extends AbstractTest
{
    public function testGetParameterList(): void {
        $this->buildRequest(
            Credentials::ADMIN, Actions::GET, Routes::PARAMETER, Response::HTTP_OK
        );
    }

    public function testPostParameter(): void {
        $this->buildRequest(
            Credentials::ADMIN, Actions::POST, Routes::PARAMETER, Response::HTTP_METHOD_NOT_ALLOWED, []
        );
    }

    public function testGetParameterItem(): void {
        $parameter = self::getRepository(Parameter::class)->findAll()[0];
        $this->buildRequest(
            Credentials::ADMIN, Actions::GET, Routes::PARAMETER.'/'.$parameter->getUuid(), Response::HTTP_OK
        );
    }

    public function testPutParameterItem(): void {
        $currency = Utils::getCurrency('Euro');
        $parameter = self::getRepository(Parameter::class)->findOneBy(['language'=>'English']);
        $response = $this->buildRequest(
            Credentials::ADMIN, Actions::PUT, Routes::PARAMETER.'/'.$parameter->getUuid(), Response::HTTP_OK,
            [ 'language' => $updatedLanguage = 'French',
            'currency' => '/currencies/'.$currency->getUuid()->toString() ]
        );
        self::assertSame($response['language'], $updatedLanguage);
        self::assertSame($response['currency']['name'], $currency->getName());
    }

    public function testDeleteParameterItem(): void {
        $parameter = self::getRepository(Parameter::class)->findAll()[0];
        $this->buildRequest(
            Credentials::ADMIN, Actions::DELETE, Routes::PARAMETER.'/'.$parameter->getUuid(), Response::HTTP_METHOD_NOT_ALLOWED
        );
    }
}
