<?php

namespace App\Tests\Api;

use App\Entity\Rules;
use App\Entity\Trade;
use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use App\Tests\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class RulesTest extends AbstractTest
{

    public function testGetRules(): void {
        $traderRule=self::getRepository(Rules::class)->findRuleByTrader(Utils::getUser())[0];
        $this->buildRequest(Credentials::ADMIN, Actions::GET, Routes::RULES.'/'.$traderRule->getUuid(), Response::HTTP_OK);
    }

    public function testPostRule(): void {
        $rule=$this->buildRequest(Credentials::ADMIN, Actions::POST, Routes::RULES, Response::HTTP_CREATED, [
            'name'=>$name='some name',
            'content'=>$content='some content',
            'setup'=>'/setups/'.(Utils::getUser())->getSetups()->first()->getUuid()
        ]);
        self::assertSame($rule['name'], $name);
        self::assertSame($rule['content'], $content);
    }
    public function testPutRuleItem(): void {
        $traderRule=self::getRepository(Rules::class)->findRuleByTrader(Utils::getUser())[0];
        $rule=$this->buildRequest(Credentials::ADMIN, Actions::PUT, Routes::RULES.'/'.$traderRule->getUuid(), Response::HTTP_OK, [
            'name'=>$updatedName='new name'
        ]);
        self::assertSame($rule['name'], $updatedName);
    }
}
