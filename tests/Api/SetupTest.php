<?php

namespace App\Tests\Api;

use App\Entity\Setup;
use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Body;
use App\Tests\Utils\Credentials;
use App\Tests\Utils\Post;
use App\Tests\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class SetupTest extends AbstractTest
{
    public function testGetSetup(): void{
        $setup=self::getRepository(Setup::class)->findByTrader(self::getUser(json_decode($credential=Credentials::TRADER)->username));
        $this->buildRequest($credential, Actions::GET, Routes::SETUP.'/'.$setup[0]->getUuid(), Response::HTTP_OK);
    }

    public function testPostSetupWithoutRules(): void {
        $setup=(new Post())->postSetup(Body::setupBody($name='Some name', $description='Une description', $traderUuid=Utils::getDefaultTrader()->getUuid()));
        self::assertSame($setup['name'], $name);
        self::assertSame($setup['description'], $description);
        self::assertSame($setup['trader']['uuid'], $traderUuid->toString());
    }
    public function testPostSetupWithRules(): void {
        $setup=(new Post())->postSetup(Body::setupBody(
            $name='Name test',
            $description='Description test',
            $traderUuid=Utils::getDefaultTrader()->getUuid(),
            $rules=[['name'=>'rule one', 'content'=>'content one'], ['name'=>'rule two', 'content'=>'content two']],
            $risk='4:1',
            true
        ));
        self::assertTrue(sizeof($setup['rules'])===sizeof($rules));
        self::assertSame($setup['name'], $name);
        self::assertSame($setup['description'], $description);
        self::assertSame($setup['trader']['uuid'], $traderUuid->toString());
        self::assertSame($setup['riskAllowed'], $risk);
        self::assertTrue($setup['favorite']);
    }

    public function testPutSetup(): void
    {
        $credential=$this->getTraderCredential();

        $setup=self::getRepository(Setup::class)->findByTrader(self::getUser(json_decode($credential)->username))[0];
        $updatedSetup=$this->buildRequest($credential, Actions::PUT, Routes::SETUP.'/'.$setup->getUuid(), Response::HTTP_OK, ['name'=>$name='Some updated name']);
        self::assertSame($updatedSetup['name'], $name);
    }
}
