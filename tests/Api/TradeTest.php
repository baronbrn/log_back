<?php

namespace App\Tests\Api;

use App\Entity\Category;
use App\Entity\Platform;
use App\Entity\Trade;
use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use App\Tests\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class TradeTest extends AbstractTest
{

    public function testGetTrade(): void {
        $this->buildRequest(
            Credentials::TRADER, Actions::GET, Routes::TRADE . '/' . Utils::getTrades()[0]->getUuid(), Response::HTTP_OK
        );
    }

    public function testPostTrade(): void {
        $platform = Utils::getRepository(Platform::class)->findAll();

        $category = Utils::getRepository(Category::class)->findOneBy(['name' => Category::SPOT]);

        $trade = $this->buildRequest(
            Credentials::TRADER, Actions::POST, Routes::TRADE, Response::HTTP_CREATED,
            [
            'type' => Trade::TYPE_LONG,
            'entry' => 100,
            'stopLoss' => 50,
            'takeProfit' => 200,
            'neededInput' => 1000,
            'timeframe' => '1D',
            'startDate' => Utils::dateToString(new \DateTime('+5 hours')),
            'endDate' => Utils::dateToString(new \DateTime('+1 day')),
            'win' => 100,
            'loss' => 50,
            'passed' => false,
            'asset' => '/assets/' . Utils::getAssets()[0]->getUuid(),
            'status' => Trade::STATUS_LOSS,
            'walletBalance' => 950,
            'riskReward' => '2:1',
            'category' => '/categories/' . Utils::getCategory(Category::SPOT)->getUuid()->toString(),
            'trader' => '/users/' . ($user = Utils::getUser())->getUuid(),
            'setup' => '/setups/' . $user->getSetups()->first()->getUuid(),
            'platform' => '/platforms/' . $platform[0]->getUuid()
        ]);
    }


    public function testApiPutTrade(): void
    {
        $trade = Utils::getTrades()[0];
        $newAsset = Utils::getAssets()[0];

        self::assertTrue($newAsset->getId() != $trade->getId());

        $res = $this->buildRequest(
            Credentials::TRADER, Actions::PUT, Routes::TRADE . '/' . $trade->getUuid(), Response::HTTP_OK,
            [
            'entry' => $updatedEntry = $trade->getEntry() + 5,
            'type' => $updatedType = $trade->isLong() ? Trade::TYPE_SHORT : Trade::TYPE_LONG,
            'neededInput' => $updatedNeededInput = $trade->getNeededInput() + 100,
            'asset' => '/assets/' . $newAsset->getUuid()->toString()
        ]);
        self::assertSame((float)$res['entry'], (float)$updatedEntry);
        self::assertSame($res['type'], $updatedType);
        self::assertSame((float)$res['neededInput'], (float)$updatedNeededInput);
        self::assertSame($res['asset']['uuid'], $newAsset->getUuid()->toString());
    }

}
