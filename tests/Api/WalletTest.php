<?php

namespace App\Tests\Api;

use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use App\Tests\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class WalletTest extends AbstractTest
{
    public function testGetWalletList(): void {
        $this->buildRequest(Credentials::ADMIN, Actions::GET, Routes::WALLET, Response::HTTP_OK);
    }
    public function testGetWalletItem(): void {
        $wallets=Utils::getWallets();
        $this->buildRequest(Credentials::ADMIN, Actions::GET, Routes::WALLET.'/'.$wallets[0]->getUuid(), Response::HTTP_OK);
    }
    public function testPutWallet(): void {
        $wallets=Utils::getWallets();
        $this->buildRequest(Credentials::ADMIN, Actions::PUT, Routes::WALLET.'/'.$wallets[0]->getUuid(), Response::HTTP_OK, [
            'balance'=>10
        ]);
    }
    public function testDeleteWallet(): void {
        $wallets=Utils::getWallets();
        $this->buildRequest(Credentials::TRADER, Actions::DELETE, Routes::WALLET.'/'.$wallets[0]->getUuid(), Response::HTTP_FORBIDDEN);
    }
}
