<?php

namespace App\Tests\DoctrineExtension;

use App\Entity\Notification;
use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use App\Tests\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class NotificationExtensionTest extends AbstractTest
{
    public function testGetNotificationList(): void {
        $trader = Utils::getDefaultTrader();
        $notifications = $this->buildRequest(
            Credentials::TRADER,
            Actions::GET,
            Routes::NOTIFICATION,
            Response::HTTP_OK
        );
        foreach ($notifications['hydra:member'] as $item) {
            $object = Utils::getRepository(Notification::class)->findOneBy(['uuid'=>$item['uuid']]);
            $trader->getNotifications()->contains($object);
        }
    }
}
