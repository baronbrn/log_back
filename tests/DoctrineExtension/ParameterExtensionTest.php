<?php

namespace App\Tests\DoctrineExtension;

use App\Entity\Parameter;
use App\Entity\Setup;
use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use App\Tests\Utils\Utils;
use Monolog\Handler\Curl\Util;
use Symfony\Component\HttpFoundation\Response;

class ParameterExtensionTest extends AbstractTest
{
    /**
     * @param string $credential
     * @param int $count
     * @return void
     * @dataProvider getExtensionListParameterData
     */
    public function testExtensionListParameter(string $credential, int $count=1): void {
        $parameters = $this->buildRequest(
            $credential, Actions::GET, Routes::PARAMETER, Response::HTTP_OK
        );

        self::assertSame($parameters['hydra:totalItems'], $count);
        if ($credential != Credentials::ADMIN) {
            foreach ($parameters['hydra:member'] as $parameter) {
                $traderParameter = Utils::getUser(json_decode($credential)->username)->getParameter();
                self::assertSame($traderParameter->getId(), $parameter['id']);
            }
        }
    }
    public function getExtensionListParameterData(): \Generator
    {
        yield [Credentials::TRADER];
        yield [Credentials::TRADER2];
        yield [Credentials::ADMIN, count(Utils::getRepository(Parameter::class)->findAll())];
    }
}
