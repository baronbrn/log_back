<?php

namespace App\Tests\DoctrineExtension;

use App\Entity\Setup;
use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use Symfony\Component\HttpFoundation\Response;

class SetupExtensionTest extends AbstractTest
{

    /**
     * @param string $credential
     * @return void
     * @throws \JsonException
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @dataProvider getTradersCredential
     */
    public function testGetListSetup(string $credential)
    {
        $setups=$this->createClientWithCredentials($credential)->request(
            Actions::GET,
            Routes::SETUP
        );
        self::assertResponseStatusCodeSame(Response::HTTP_OK);
        $setups=self::decode($setups);
        $user=self::getUser(json_decode($credential)->username);

        foreach ($setups['hydra:member'] as $setupss) {
            $setupEntity=self::getRepository(Setup::class)->findOneBy(['id'=>$setupss['id']]);
            self::assertTrue($user->getSetups()->contains($setupEntity));
        }
    }
}
