<?php

namespace App\Tests\DoctrineExtension;

use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class TradeExtensionTest extends AbstractTest
{

    /**
     * @param string $credential
     * @return void
     * @throws \JsonException
     * @throws TransportExceptionInterface
     * @dataProvider getTradersCredential
     */
    public function testGetListTrade(string $credential): void
    {
        $trades=$this->createClientWithCredentials($credential)->request(
            Actions::GET,
            Routes::TRADE
        );
        self::assertResponseStatusCodeSame(Response::HTTP_OK);
        $trades=self::decode($trades);
        $user=self::getUser(json_decode($credential)->username);

        foreach ($trades['hydra:member'] as $trade) {
            self::assertSame($trade['trader']['id'], $user->getId());
        }
    }
}
