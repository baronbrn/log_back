<?php

namespace App\Tests\DoctrineExtension;

use App\Entity\Wallet;
use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class WalletExtensionTest extends AbstractTest
{

    /**
     * @param string $credential
     * @return void
     * @dataProvider getTradersCredential
     */
    public function testGetWalletListExtension(string $credential): void {
        $wallets=$this->buildRequest($credential, Actions::GET, Routes::WALLET, Response::HTTP_OK);
        $user=Utils::getUser(json_decode($credential)->username);
        foreach ($wallets['hydra:member'] as $wallet) {
            $walletEntity=self::getRepository(Wallet::class)->findOneBy(['id'=>$wallet['id']]);
            self::assertTrue($user->getWallets()->contains($walletEntity));
        }
    }
}
