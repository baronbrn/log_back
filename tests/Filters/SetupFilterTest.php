<?php

namespace App\Tests\Filters;

use App\Entity\Asset;
use App\Entity\Category;
use App\Entity\Trade;
use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use App\Tests\Utils\Utils;
use EasyRdf\Literal\Date;
use Symfony\Component\HttpFoundation\Response;

class SetupFilterTest extends AbstractTest
{

    public function testUserGetSetupByDateOfCreation(): void
    {
        $setups = $this->buildRequest(
            Credentials::TRADER, Actions::GET, Routes::SETUP, Response::HTTP_OK
        );
        $keepDate = new \DateTime($setups['hydra:member'][0]['createdAt']);
        foreach ($setups['hydra:member'] as $key => $setup) {
            if ($key < 1) continue ;

            self::assertTrue($keepDate > $setup['createdAt']);
            $keepDate = new \DateTime($setup['createdAt']);
        }
    }
}
