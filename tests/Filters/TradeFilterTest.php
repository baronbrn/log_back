<?php

namespace App\Tests\Filters;

use App\Entity\Asset;
use App\Entity\Category;
use App\Entity\Trade;
use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use App\Tests\Utils\Utils;

class TradeFilterTest extends AbstractTest
{

    /**
     * @return void
     * @throws \JsonException
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @dataProvider getTradersCredential
     */
    public function testGetTradePerUser(string $credential): void
    {
        self::assertNotNull($trader=self::getUser(json_decode($credential)->username));

        $userTrades=Utils::getRepository(Trade::class)->findByTrader($trader);

        $res=$this->createClientWithCredentials($credential)->request(Actions::GET, Routes::TRADE);
        $res=self::decode($res);

        self::assertSame(count($userTrades), $res['hydra:totalItems']);

        foreach ($res['hydra:member'] as $trade) {
            /** @var Trade $currentTrade */
            $currentTrade=self::getRepository(Trade::class)->findOneBy(['id'=>$trade['id']]);
            self::assertSame($trader->getEmail(), $currentTrade->getTrader()->getEmail());
        }
    }

    /**
     * @param string $status
     * @return void
     * @throws \JsonException
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @dataProvider getStatusData
     */
    public function testGetTradePerStatus(string $status): void {
        $trader = self::getUser();
        $userTradeByStatus = Utils::getRepository(Trade::class)->findByUserAndStatus($trader, $status);
        $route=Routes::TRADE.'?status='.$status;

        $response = $this->createClientWithCredentials(Credentials::TRADER)->request(
            Actions::GET, $route
        );
        $res = self::decode($response);
        self::assertSame(count($userTradeByStatus), $res['hydra:totalItems']);
        foreach ($res['hydra:member'] as $trade) {
            self::assertSame($trade['status'], $status);
        }
    }
    public function getStatusData(): \Generator {
        yield[Trade::STATUS_NEUTRAL];
        yield[Trade::STATUS_IN_PROGRESS];
        yield[Trade::STATUS_LOSS];
        yield[Trade::STATUS_WIN];
    }

    /**
     * @param Category $category
     * @return void
     * @throws \JsonException
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @dataProvider getCategoryData
     */
    public function testGetTradeByCategory(Category $category): void {
        $trader = self::getUser();
        $userTradeByCategory = Utils::getRepository(Trade::class)->findByUserAndCategory($trader, $categoryName=$category->getName());
        $route=Routes::TRADE.'?category.name='.$categoryName;


        $response = $this->createClientWithCredentials(Credentials::TRADER)->request(
            Actions::GET, $route
        );
        $res = self::decode($response);
        self::assertSame(count($userTradeByCategory), $res['hydra:totalItems']);
        foreach ($res['hydra:member'] as $trade) {
            self::assertSame($trade['category']['name'], $categoryName);
        }
    }
    public function getCategoryData(): \Generator {
        $categories = Utils::getCategories();
        foreach ($categories as $category) {
            yield[$category];
        }
    }

    /**
     * @param Asset $asset
     * @return void
     * @throws \JsonException
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @dataProvider getAssetData
     */
    public function testGetTradeByAsset(Asset $asset): void {
        $trader = self::getUser();
        $userTradeByAsset = Utils::getRepository(Trade::class)->findByUserAndAsset($trader, $assetName=$asset->getName());
        $route=Routes::TRADE.'?asset.name='.$assetName;

        $response = $this->createClientWithCredentials(Credentials::TRADER)->request(
            Actions::GET, $route
        );
        $res = self::decode($response);
        self::assertSame(count($userTradeByAsset), $res['hydra:totalItems']);
        foreach ($res['hydra:member'] as $trade) {
            self::assertSame($trade['asset']['name'], $assetName);
        }
    }

    public function getAssetData(): \Generator {
        $assets = Utils::getRepository(Asset::class)->findAll();
        foreach (array_slice($assets, 0, 5) as $asset) {
            yield[$asset];
        }
    }

    /**
     * @return void
     * @dataProvider getCategoryAndStatus
     */
    public function testGetTradeByCategoryAndStatus(string $status, Category $category) {
        $trader = self::getUser();
        $userTradeByCategoryAndStatus = Utils::getRepository(Trade::class)->findByUserAndCategoryAndStatus($trader, $categoryName=$category->getName(), $status);
        $route=Routes::TRADE.'?category.name='.$categoryName.'&status='.$status;

        $response = $this->createClientWithCredentials(Credentials::TRADER)->request(
            Actions::GET, $route
        );
        $res = self::decode($response);

        self::assertSame(count($userTradeByCategoryAndStatus), $res['hydra:totalItems']);
        foreach ($res['hydra:member'] as $trade) {
            self::assertSame($trade['category']['name'], $categoryName);
            self::assertSame($trade['status'], $status);
        }
    }
    public function getCategoryAndStatus(): \Generator {
        $categories = Utils::getCategories();
        foreach ($categories as $category) {
            yield[Trade::STATUS_NEUTRAL, $category];
            yield[Trade::STATUS_WIN, $category];
            yield[Trade::STATUS_LOSS, $category];
            yield[Trade::STATUS_IN_PROGRESS, $category];
        }
    }

}
