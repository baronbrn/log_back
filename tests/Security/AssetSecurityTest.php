<?php

namespace App\Tests\Security;

use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use App\Tests\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class AssetSecurityTest extends AbstractTest
{
    public function getCredentialAuthorized(): \Generator {
        yield [Credentials::ADMIN, Response::HTTP_OK];
    }
    public function getCredentialUnauthorized(): \Generator {
        yield [Credentials::TRADER, Response::HTTP_FORBIDDEN];
    }

    /**
     * @param string $credential
     * @param int $expected
     * @return void
     * @dataProvider getCredentialAuthorized
     * @dataProvider getCredentialUnauthorized
     */
    public function testPostAsset(string $credential, int $expected): void
    {
        $expected = ($expected != 200) ? $expected : Response::HTTP_CREATED;
        $this->buildRequest($credential, Actions::POST, Routes::ASSET, $expected,
            [ 'name' => 'asset name', 'symbol' => 'symbol' ]
        );
    }

    /**
     * @param string $credential
     * @param int $expected
     * @return void
     * @dataProvider getCredentialAuthorized
     * @dataProvider getCredentialUnauthorized
     */
    public function testPutAsset(string $credential, int $expected): void
    {
        $this->buildRequest($credential, Actions::PUT, Routes::ASSET . '/' . Utils::getAssets()[0]->getUuid(), $expected,
            [ 'name'=>'new name' ]
        );
    }

    /**
     * @param string $credential
     * @param int $expected
     * @return void
     * @dataProvider getCredentialUnauthorized
     */
    public function testDeleteAsset(string $credential, int $expected): void
    {
        $this->buildRequest(
            $credential, Actions::DELETE, Routes::ASSET . '/' . Utils::getAssets()[0]->getUuid(), $expected);
    }
}
