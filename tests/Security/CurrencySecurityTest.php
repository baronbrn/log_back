<?php

namespace App\Tests\Security;

use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use Symfony\Component\HttpFoundation\Response;

class CurrencySecurityTest extends AbstractTest
{
    public function getDataSecurityPostCurrency(): \Generator
    {
        yield [Credentials::ADMIN, Response::HTTP_CREATED];
        yield [Credentials::TRADER];
    }

    /**
     * @param string $credential
     * @param int $expectedResponse
     * @return void
     * @dataProvider getDataSecurityPostCurrency
     */
    public function testSecurityPostCurrency(string $credential, int $expectedResponse = Response::HTTP_FORBIDDEN): void {
        $this->buildRequest(
            $credential, Actions::POST, Routes::CURRENCY, $expectedResponse,
            ['name' => 'new currency name']
        );
    }
}
