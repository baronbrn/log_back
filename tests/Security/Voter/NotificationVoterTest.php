<?php

namespace App\Tests\Security\Voter;

use App\Entity\Notification;
use App\Entity\Setup;
use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use App\Tests\Utils\Utils;
use Generator;
use Symfony\Component\HttpFoundation\Response;

class NotificationVoterTest extends AbstractTest
{
    public function getListCredentials(): Generator {
        yield [Credentials::TRADER];
        yield [Credentials::TRADER2];
    }

    /**
     * @param string $credential
     * @return void
     * @dataProvider getListCredentials
     */
    public function testSecurityGetNotificationList(string $credential): void {
        $this->buildRequest(
            $credential, Actions::GET,
            Routes::NOTIFICATION, Response::HTTP_OK
        );
    }

    public function getItemCredentials(): Generator {
        $notification=Utils::getNotification(Utils::getDefaultTrader())[0];

        yield [Credentials::TRADER, $notification, Response::HTTP_OK];
        yield [Credentials::TRADER2, $notification, Response::HTTP_NOT_FOUND];
    }

    /**
     * @return void
     * @dataProvider getItemCredentials
     */
    public function testSecurityGetNotificationItem(
        string $credentials,
        Notification $notification,
        int $expected
    ): void {
        $this->buildRequest(
            $credentials, Actions::GET,
            Routes::NOTIFICATION.'/'.$notification->getUuid(),
            $expected
        );
    }

    /**
     * @param string $credentials
     * @param Notification $notification
     * @param int $expected
     * @return void
     * @dataProvider getItemCredentials
     */
    public function testSecurityPutNotification(
        string $credentials,
        Notification $notification,
        int $expected
    ){
        $this->buildRequest(
            $credentials,
            Actions::PUT,
            Routes::NOTIFICATION.'/'.$notification->getUuid(),
            $expected,
            ['content'=>'new content']
        );
    }
}
