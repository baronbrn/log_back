<?php

namespace App\Tests\Security\Voter;

use App\Entity\Parameter;
use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use App\Tests\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class ParameterVoterTest extends AbstractTest
{
    public function getGetParameterListData(): \Generator
    {
        yield[Credentials::ADMIN];
        yield[Credentials::TRADER];
    }

    /**
     * @param string $credentials
     * @param int $expectedResponseCode
     * @return void
     * @dataProvider getGetParameterListData
     */
    public function testGetParameterList(string $credentials, int $expectedResponseCode=Response::HTTP_OK): void {
        $this->buildRequest(
            $credentials, Actions::GET, Routes::PARAMETER, $expectedResponseCode
        );
    }

    public function getGetParameterItemData(): \Generator
    {
        $defaultTraderParameter = Utils::getDefaultTrader()->getParameter();
        yield[Credentials::TRADER, $defaultTraderParameter, Response::HTTP_OK];
        yield[Credentials::ADMIN, $defaultTraderParameter, Response::HTTP_OK];
        yield[Credentials::TRADER2, $defaultTraderParameter, Response::HTTP_NOT_FOUND];
    }

    /**
     * @param string $credential
     * @param Parameter $parameter
     * @param int $responseStatusCode
     * @return void
     * @dataProvider getGetParameterItemData
     */
    public function testGetParameterItem(string $credential, Parameter $parameter, int $responseStatusCode = Response::HTTP_OK): void {
        $this->buildRequest(
            $credential, Actions::GET, Routes::PARAMETER . '/' . $parameter->getUuid(), $responseStatusCode
        );
    }

    public function getPutParameterItemData(): \Generator
    {
        $defaultTraderParameter = Utils::getDefaultTrader()->getParameter();
        yield[Credentials::TRADER, $defaultTraderParameter, Response::HTTP_OK];
        $randomParameter = Utils::getRepository(Parameter::class)->findAll()[0];
        yield[Credentials::ADMIN, $randomParameter, Response::HTTP_OK];
        yield[Credentials::TRADER2, $defaultTraderParameter, Response::HTTP_NOT_FOUND];
    }

    /**
     * @param string $credential
     * @param Parameter $parameter
     * @param int $responseStatusCode
     * @return void
     * @dataProvider getPutParameterItemData
     */
    public function testPutParameterItem(string $credential, Parameter $parameter, int $responseStatusCode = Response::HTTP_OK): void
    {
        $this->buildRequest(
            $credential, Actions::PUT,Routes::PARAMETER . '/' . $parameter->getUuid(), $responseStatusCode,
            []
        );
    }
}
