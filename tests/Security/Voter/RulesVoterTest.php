<?php

namespace App\Tests\Security\Voter;

use App\Entity\Rules;
use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use App\Tests\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class RulesVoterTest extends AbstractTest
{
    public function getGetPutRulesData(): \Generator {
        $traderRule=self::getRepository(Rules::class)->findRuleByTrader(Utils::getUser())[0];
        yield [Credentials::ADMIN, $traderRule, Response::HTTP_OK];
        yield [Credentials::TRADER, $traderRule, Response::HTTP_OK];
        yield [Credentials::TRADER2, $traderRule, Response::HTTP_FORBIDDEN];
    }

    /**
     * @param string $credential
     * @param Rules $rule
     * @param int $expected
     * @return void
     * @dataProvider getGetPutRulesData
     */
    public function testGetRules(string $credential, Rules $rule, int $expected): void {
        $this->buildRequest($credential, Actions::GET, Routes::RULES.'/'.$rule->getUuid(), $expected);
    }

    /**
     * @param string $credential
     * @param Rules $rule
     * @param int $expected
     * @return void
     * @dataProvider getGetPutRulesData
     */
    public function testPutRuleItem(string $credential, Rules $rule, int $expected): void {
        $rule=$this->buildRequest($credential, Actions::PUT, Routes::RULES.'/'.$rule->getUuid(), $expected, [
            'name'=>$updatedName='new name'
        ]);
        if ($expected===Response::HTTP_OK)
            self::assertSame($rule['name'], $updatedName);
    }

    public function getDeleteRulesData(): \Generator {
        $traderRule=self::getRepository(Rules::class)->findAll()[0];
        yield [Credentials::ADMIN, $traderRule, Response::HTTP_OK];
        $traderRule=self::getRepository(Rules::class)->findRuleByTrader(Utils::getUser())[0];
        yield [Credentials::TRADER, $traderRule, Response::HTTP_OK];
        $traderRule=self::getRepository(Rules::class)->findRuleByTrader(Utils::getUser())[1];
        yield [Credentials::TRADER2, $traderRule, Response::HTTP_FORBIDDEN];
    }

    /**
     * @return void
     * @dataProvider getDeleteRulesData
     */
    public function testDeleteRule(string $credential, Rules $rules, int $expected) {
        $expected= ($expected===200) ? Response::HTTP_NO_CONTENT : $expected;
        $this->buildRequest($credential, Actions::DELETE, Routes::RULES.'/'.$rules->getUuid(), $expected);
    }
}
