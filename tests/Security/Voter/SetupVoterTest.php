<?php

namespace App\Tests\Security\Voter;

use App\Entity\Setup;
use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use Symfony\Component\HttpFoundation\Response;

class SetupVoterTest extends AbstractTest
{
    public function getPutSetupData()
    {
        $credentialAuthorized=Credentials::TRADER;
        $user=self::getUser(json_decode($credentialAuthorized)->username);
        $setup=$user->getSetups()->first();
        yield [$credentialAuthorized, $setup];
        yield [Credentials::ADMIN, $setup];
        yield [Credentials::TRADER2, $setup, Response::HTTP_FORBIDDEN];
    }
    /**
     * @return void
     * @throws \JsonException
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @dataProvider getPutSetupData
     */
    public function testPutSetup(string $credential, Setup $setup, int $expected=Response::HTTP_OK): void
    {
        $res=$this->createClientWithCredentials($credential)->request(
            Actions::PUT,
            Routes::SETUP.'/'.$setup->getUuid(),
            ['json'=>[
                'name'=>$updatedName='new name'
            ]]
        );
        self::assertResponseStatusCodeSame($expected);
        if ($expected===Response::HTTP_OK)
            self::assertSame((self::decode($res))['name'], $updatedName);
    }

    public function getDeleteSetupData(): \Generator
    {
        $credentialAuthorized=Credentials::TRADER;
        $user=self::getUser(json_decode($credentialAuthorized)->username);
        $setup=$user->getSetups()->first();
        //yield [$credentialAuthorized, $setup];
        //yield [Credentials::ADMIN, $setup];
        yield [Credentials::TRADER2, $setup, Response::HTTP_FORBIDDEN];
    }

    /**
     * @param string $credential
     * @param Setup $setup
     * @param int $expected
     * @return void
     * @throws \JsonException
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @dataProvider getDeleteSetupData
     */
    public function testDeleteSetup(string $credential, Setup $setup, int $expected=Response::HTTP_NO_CONTENT): void
    {
        $this->createClientWithCredentials($credential)->request(Actions::DELETE, Routes::SETUP.'/'.$setup->getUuid());
        self::assertResponseStatusCodeSame($expected);
    }

}
