<?php

namespace App\Tests\Security\Voter;

use App\Entity\Setup;
use App\Entity\Trade;
use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use Symfony\Component\HttpFoundation\Response;

class TradeVoterTest extends AbstractTest
{
    public function getPutAndGetTradeData(): \Generator
    {
        $credentialAuthorized=Credentials::TRADER;
        $user=self::getUser(json_decode($credentialAuthorized)->username);
        $trade=$user->getTrades()->first();
        yield [$credentialAuthorized, $trade];
        yield [Credentials::ADMIN, $trade];
        yield [Credentials::TRADER2, $trade, Response::HTTP_FORBIDDEN];
    }

    /**
     * @return void
     * @dataProvider getPutAndGetTradeData
     */
    public function testGetTrade(string $credential, Trade $trade, int $expected=Response::HTTP_OK): void
    {
        $this->createClientWithCredentials($credential)->request(Actions::GET, Routes::TRADE.'/'.$trade->getUuid());
        self::assertResponseStatusCodeSame($expected);
    }

    /**
     * @return void
     * @throws \JsonException
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @dataProvider getPutAndGetTradeData
     */
    public function testPutTrade(string $credential, Trade $trade, int $expected=Response::HTTP_OK): void
    {
        $res=$this->createClientWithCredentials($credential)->request(
            Actions::PUT,
            Routes::TRADE.'/'.$trade->getUuid(),
            ['json'=>[
                'entry'=>$updatedEntry=150
            ]]
        );
        self::assertResponseStatusCodeSame($expected);
        if ($expected===Response::HTTP_OK)
            self::assertSame((self::decode($res))['entry'], $updatedEntry);
    }

    public function getDeleteTradeData(): \Generator
    {
        $credentialAuthorized=Credentials::TRADER;
        $user=self::getUser(json_decode($credentialAuthorized)->username);
        $trade=$user->getTrades()->first();
        //yield [$credentialAuthorized, $trade];
        //yield [Credentials::ADMIN, $trade];
        yield [Credentials::TRADER2, $trade, Response::HTTP_FORBIDDEN];
    }

    /**
     * @param string $credential
     * @param Trade $trade
     * @param int $expected
     * @return void
     * @throws \JsonException
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @dataProvider getDeleteTradeData
     */
    public function testDeleteTrade(string $credential, Trade $trade, int $expected=Response::HTTP_NO_CONTENT): void
    {
        $this->createClientWithCredentials($credential)->request(Actions::DELETE, Routes::TRADE.'/'.$trade->getUuid());
        self::assertResponseStatusCodeSame($expected);
    }

}
