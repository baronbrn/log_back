<?php

namespace App\Tests\Security\Voter;

use App\Entity\User;
use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use App\Tests\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class UserVoterTest extends AbstractTest
{
    public function getListUserData(): \Generator {
        yield [Credentials::ADMIN];
        yield [Credentials::TRADER, Response::HTTP_FORBIDDEN];
    }
    /**
     * @return void
     * @dataProvider getListUserData
     */
    public function testGetUserList(string $credential, int $expected=Response::HTTP_OK): void {
        $this->buildRequest($credential, Actions::GET, Routes::USER, $expected);
    }

    public function getPutAndGetUserData(): \Generator {
        $user=Utils::getUser(json_decode($credential=Credentials::TRADER)->username);

        yield [Credentials::ADMIN, $user];
        yield [$credential, $user];
        yield [Credentials::TRADER2, $user, Response::HTTP_FORBIDDEN];
    }
    /**
     * @param string $credential
     * @param User $user
     * @param int $expected
     * @return void
     * @dataProvider getPutAndGetUserData
     */
    public function testGetUserItem(string $credential, User $user, int $expected=Response::HTTP_OK): void {
        $this->buildRequest($credential, Actions::GET, Routes::USER.'/'.$user->getUuid(), $expected);
    }

    /**
     * @param string $credential
     * @param User $user
     * @param int $expected
     * @return void
     * @throws \JsonException
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @dataProvider getPutAndGetUserData
     */
    public function testPutUser(string $credential, User $user, int $expected=Response::HTTP_OK): void {
        $this->createClientWithCredentials($credential)->request(
            Actions::PUT,
            Routes::USER.'/'.$user->getUuid(),
            ['json'=>[ ]]
        );
        self::assertResponseStatusCodeSame($expected);
    }

    public function getDeleteUserData(): \Generator {
        yield [$credential=Credentials::TRADER2, $user=Utils::getUser(json_decode($credential)->username)];
        yield [$credential=Credentials::TRADER, $user=Utils::getUser(json_decode($credential)->username)];
    }
    /**
     * @param string $credential
     * @param User $user
     * @param int $expected
     * @return void
     * @dataProvider getDeleteUserData
     */
    public function testDeleteUser(string $credential, User $user, int $expected=Response::HTTP_FORBIDDEN) {
        $this->buildRequest($credential, Actions::DELETE, Routes::USER.'/'.$user->getUuid(), $expected);
    }
}
