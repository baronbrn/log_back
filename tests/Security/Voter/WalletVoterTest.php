<?php

namespace App\Tests\Security\Voter;

use App\Entity\Wallet;
use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use App\Tests\Utils\Credentials;
use App\Tests\Utils\Utils;
use Symfony\Component\HttpFoundation\Response;

class WalletVoterTest extends AbstractTest
{
    public function getGetWalletDataItem(): \Generator {
        $wallet=Utils::getWallets()[0];
        yield [Credentials::ADMIN, $wallet];
        yield [Credentials::TRADER, $wallet];
        yield [Credentials::TRADER2, $wallet, Response::HTTP_FORBIDDEN];
    }
    /**
     * @param string $credentials
     * @param Wallet $wallet
     * @param int $expected
     * @return void
     * @dataProvider getGetWalletDataItem
     */
    public function testGetWalletItem(string $credentials, Wallet $wallet, int $expected=Response::HTTP_OK): void {
        $this->buildRequest($credentials, Actions::GET, Routes::WALLET.'/'.$wallet->getUuid(), $expected);
    }

    public function getPutWalletData(): \Generator
    {
        $wallet = Utils::getDefaultTrader()->getWallets()[0];
        yield [Credentials::TRADER, $wallet];
        yield [Credentials::TRADER2, $wallet];
        yield [Credentials::ADMIN, $wallet, Response::HTTP_OK];
    }
    /**
     * @param string $credentials
     * @param Wallet $wallet
     * @param int $expected
     * @return void
     * @dataProvider getPutWalletData
     */
    public function testPutWallet(string $credentials, Wallet $wallet, int $expected=Response::HTTP_FORBIDDEN): void {
        $this->buildRequest($credentials, Actions::PUT, Routes::WALLET.'/'.$wallet->getUuid(), $expected,
            [ 'balance'=>120 ]
        );
    }

    public function getDeleteWalletData(): \Generator
    {
        $wallet = Utils::getDefaultTrader()->getWallets()[0];
        yield [Credentials::TRADER, $wallet];
        yield [Credentials::TRADER2, $wallet];
    }
    /**
     * @param string $credentials
     * @param Wallet $wallet
     * @param int $expected
     * @return void
     * @dataProvider getDeleteWalletData
     */
    public function testDeleteWallet(string $credentials, Wallet $wallet, int $expected=Response::HTTP_FORBIDDEN): void {
        $this->buildRequest($credentials, Actions::DELETE, Routes::WALLET.'/'.$wallet->getUuid(), $expected);
    }
}
