<?php

namespace App\Tests\Utils;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;

class Body extends ApiTestCase
{
    public static function setupBody(
        string $name=null,
        string $description=null,
        string $traderUuid=null,
        array $rules=null,
        string $riskAllowed=null,
        bool $favorite=null
    ): array {
        return [
            'name'=>$name?:'Some name',
            'description'=>$description?:'Une description',
            'rules'=>$rules ?: [
                ['name'=>'rule one', 'content'=>'ONE'],
                ['name'=>'rule two', 'content'=>'TWO'],
                ['name'=>'rule three', 'content'=>'THREE']
            ],
            'trader'=> $traderUuid ?'/users/'.$traderUuid: '/users/'.(Utils::getDefaultTrader())->getUuid()->toString(),
            'riskAllowed'=> $riskAllowed ?: '2:1',
            'favorite'=>$favorite ?: false
        ];
    }
}
