<?php

namespace App\Tests\Utils;

class Credentials
{
    public const ADMIN='{ "username": "admin@gmail.com", "password": "password" }';

    public const TRADER='{ "username": "trader0@gmail.com", "password": "password" }';
    public const TRADER2='{ "username": "trader1@gmail.com", "password": "password" }';

    public static function buildCredential(string $email, string $password='password'): string
    {
        return '{ "username": "'.$email.'", "password": "password" }';
    }
}
