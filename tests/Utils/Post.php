<?php

namespace App\Tests\Utils;

use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use Symfony\Component\HttpFoundation\Response;

class Post extends AbstractTest
{
    public function postSetup(
        array $body,
        int $response=Response::HTTP_CREATED,
        string $credential=Credentials::TRADER
    )
    {
        return $this->buildRequest(
            $credential,
            Actions::POST,
            Routes::SETUP,
            $response,
            $body ?: Body::setupBody()
        );
    }

    public function postTrade(
        array $body,
        int $response=Response::HTTP_CREATED,
        string $credential=Credentials::TRADER
    ){
        return $this->buildRequest(
            $credential,
            Actions::POST,
            Routes::TRADE,
            $response,
            $body ?: Body::setupBody()
        );
    }
}
