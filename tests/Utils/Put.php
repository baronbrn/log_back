<?php

namespace App\Tests\Utils;

use App\Generic\Actions;
use App\Generic\Routes;
use App\Tests\AbstractTest;
use Symfony\Component\HttpFoundation\Response;

class Put extends AbstractTest
{
    public function putTrade(
        array $body,
        $tradeUuid,
        int $response=Response::HTTP_CREATED,
        string $credential=Credentials::TRADER
    ){
        return $this->buildRequest(
            $credential,
            Actions::PUT,
            Routes::TRADE.'/'.$tradeUuid,
            $response,
            $body
        );
    }
}
