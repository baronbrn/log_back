<?php

namespace App\Tests\Utils;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Asset;
use App\Entity\Category;
use App\Entity\Currency;
use App\Entity\Notification;
use App\Entity\Trade;
use App\Entity\User;
use App\Entity\Wallet;
use DateTime;

class Utils extends ApiTestCase
{
    public function setUp(): void {
        self::bootKernel();
    }
    public static function getRepository(string $class) {
        return static::getContainer()->get('doctrine')->getRepository($class);
    }
    public static function dateToString(DateTime $date, string $format = 'Y-m-d H:i:s'): string {
        return $date->format($format);
    }

    public static function getDefaultTrader(): User {
        return self::getUser(json_decode(Credentials::TRADER)->username);
    }
    public static function getUser(?string $email=null): User {
        $email = ($email) ?: json_decode(Credentials::TRADER)->username;

        return self::getRepository(User::class)->findByEmail($email);
    }
    public static function getTrades(?User $user=null) {
        return self::getRepository(Trade::class)->findByTrader(
            $user?:self::getDefaultTrader()
        );
    }
    public static function getCategories() {
        return self::getRepository(Category::class)->findAll();
    }
    public static function getCategory(string $name) {
        return self::getRepository(Category::class)->findOneBy(['name' => $name]);
    }

    public static function getAsset(string $name) {
        return self::getRepository(Asset::class)->findOneBy(['name' => $name]);
    }
    public static function getAssets(){
        return self::getRepository(Asset::class)->findAll();
    }

    public static function getWallets(?User $user = null) {
        return self::getRepository(Wallet::class)->findByTrader(
            $user?:self::getDefaultTrader()
        );
    }

    public static function getNotification(User $user) {
        return self::getRepository(Notification::class)->findBy(['trader'=>$user]);
    }
    public static function getNotifications(){
        return self::getRepository(Notification::class)->findAll();
    }

    public static function getCurrency(string $name) {
        return self::getRepository(Currency::class)->findOneBy(['name' => $name]);
    }
    public static function getCurrencies(){
        return self::getRepository(Currency::class)->findAll();
    }
}
