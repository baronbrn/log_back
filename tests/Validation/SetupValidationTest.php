<?php

namespace App\Tests\Validation;

use App\Tests\AbstractTest;
use App\Tests\Utils\Body;
use App\Tests\Utils\Post;
use Generator;
use Symfony\Component\HttpFoundation\Response;

class SetupValidationTest extends AbstractTest
{

    /**
     * @return void
     * @dataProvider getPostSetupValidation
     */
    public function testPostSetupValidation(array $body, $response = Response::HTTP_CREATED): void
    {
        (new Post())->postSetup($body, $response);
    }

    public function getPostSetupValidation(): Generator
    {
        yield[Body::setupBody()];
        yield[['name'=>""], Response::HTTP_UNPROCESSABLE_ENTITY];
        yield[Body::setupBody(" "), Response::HTTP_UNPROCESSABLE_ENTITY];
    }
}
