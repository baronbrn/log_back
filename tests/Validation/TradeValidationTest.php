<?php

namespace App\Tests\Validation;

use App\Entity\Category;
use App\Entity\Trade;
use App\Tests\AbstractTest;
use App\Tests\Utils\Post;
use App\Tests\Utils\Put;
use App\Tests\Utils\Utils;
use Generator;
use Symfony\Component\HttpFoundation\Response;

class TradeValidationTest extends AbstractTest
{
    /**
     * @return void
     * @dataProvider getPostTradeFieldTypeError
     * @dataProvider getPostTradeFieldWinError
     * @dataProvider getPostTradeFieldLossError
     * @dataProvider getPostTradeFieldAssetError
     * @dataProvider getPostTradeFieldCategoryError
     * @dataProvider getPostTradeFieldEntryError
     * @dataProvider getPostTradeFieldTypeError
     * @dataProvider getPostTradeFieldLeverageError
     * @dataProvider getPostTradeFieldTraderError
     */
    public function testValidationPostTradeValidation(
        array $body,
              $response = Response::HTTP_CREATED,
        string $errorMessage=null
    ): void
    {
        $res=(new Post())->postTrade($body, $response);
        if ($response!==Response::HTTP_CREATED) {
            foreach ($res['violations'] as $violation) {
                self::assertStringContainsString($violation['message'], $errorMessage);
            }
        }
    }

    public function getPostTradeFieldTypeError(): Generator
    {
        $assetUuid=Utils::getAssets()[0]->getUuid()->toString();
        $categoryUuid=Utils::getCategory(Category::SPOT)->getUuid()->toString();
        $traderUuid=Utils::getDefaultTrader()->getUuid()->toString();
        yield[
            ['entry'=>12, 'neededInput'=>20, 'win'=>20, 'loss'=>29, 'asset'=>"/assets/$assetUuid", 'category'=>"/categories/$categoryUuid", 'trader'=>"/users/$traderUuid"],
            Response::HTTP_UNPROCESSABLE_ENTITY,
            "La valeur 'type' ne peut pas être vide."
        ];
        yield[
            ['type'=>" ",'entry'=>12, 'neededInput'=>20, 'win'=>20, 'loss'=>29, 'asset'=>"/assets/$assetUuid", 'category'=>"/categories/$categoryUuid", 'trader'=>"/users/$traderUuid"],
            Response::HTTP_UNPROCESSABLE_ENTITY,
            "Le champ 'type' doit faire au moins deux charactères."
        ];
    }

    public function getPostTradeFieldEntryError(): Generator
    {
        $assetUuid=Utils::getAssets()[0]->getUuid()->toString();
        $categoryUuid=Utils::getCategory(Category::SPOT)->getUuid()->toString();
        $traderUuid=Utils::getDefaultTrader()->getUuid()->toString();
        yield[
            ['type'=>Trade::TYPE_LONG, 'neededInput'=>20, 'win'=>20, 'loss'=>29, 'asset'=>"/assets/$assetUuid", 'category'=>"/categories/$categoryUuid", 'trader'=>"/users/$traderUuid"],
            Response::HTTP_UNPROCESSABLE_ENTITY,
            "La valeur 'entry' ne peut pas être vide."
        ];
    }

    public function getPostTradeFieldWinError(): Generator
    {
        $assetUuid=Utils::getAssets()[0]->getUuid()->toString();
        $categoryUuid=Utils::getCategory(Category::SPOT)->getUuid()->toString();
        $traderUuid=Utils::getDefaultTrader()->getUuid()->toString();
        yield[
            ['type'=>Trade::TYPE_LONG, 'entry'=>12, 'neededInput'=>20, 'loss'=>29, 'asset'=>"/assets/$assetUuid", 'category'=>"/categories/$categoryUuid", 'trader'=>"/users/$traderUuid"],
            Response::HTTP_UNPROCESSABLE_ENTITY,
            "La valeur 'win' ne peut pas être vide."
        ];
    }

    public function getPostTradeFieldLossError(): Generator
    {
        $assetUuid=Utils::getAssets()[0]->getUuid()->toString();
        $categoryUuid=Utils::getCategory(Category::SPOT)->getUuid()->toString();
        $traderUuid=Utils::getDefaultTrader()->getUuid()->toString();
        yield[
            ['type'=>Trade::TYPE_LONG, 'entry'=>12, 'neededInput'=>20, 'win'=>20, 'asset'=>"/assets/$assetUuid", 'category'=>"/categories/$categoryUuid", 'trader'=>"/users/$traderUuid"],
            Response::HTTP_UNPROCESSABLE_ENTITY,
            "La valeur 'loss' ne peut pas être vide."
        ];
    }

    public function getPostTradeFieldAssetError(): Generator
    {
        $categoryUuid=Utils::getCategory(Category::SPOT)->getUuid()->toString();
        $traderUuid=Utils::getDefaultTrader()->getUuid()->toString();
        yield[
            ['type'=>Trade::TYPE_LONG, 'entry'=>12, 'neededInput'=>20, 'win'=>20, 'loss'=>29, 'category'=>"/categories/$categoryUuid", 'trader'=>"/users/$traderUuid"],
            Response::HTTP_UNPROCESSABLE_ENTITY,
            "La valeur 'asset' ne peut pas être vide."
        ];
    }

    public function getPostTradeFieldCategoryError(): Generator
    {
        $assetUuid=Utils::getAssets()[0]->getUuid()->toString();
        $traderUuid=Utils::getDefaultTrader()->getUuid()->toString();
        yield[
            ['type'=>Trade::TYPE_LONG, 'entry'=>12, 'neededInput'=>20, 'win'=>20, 'loss'=>29, 'asset'=>"/assets/$assetUuid", 'trader'=>"/users/$traderUuid"],
            Response::HTTP_UNPROCESSABLE_ENTITY,
            "La valeur 'category' ne peut pas être vide."
        ];
    }

    public function getPostTradeFieldTraderError(): Generator
    {
        $assetUuid=Utils::getAssets()[0]->getUuid()->toString();
        $categoryUuid=Utils::getCategory(Category::SPOT)->getUuid()->toString();
        yield[
            ['type'=>Trade::TYPE_LONG, 'entry'=>12, 'neededInput'=>20, 'win'=>20, 'loss'=>29, 'asset'=>"/assets/$assetUuid", 'category'=>"/categories/$categoryUuid"],
            Response::HTTP_UNPROCESSABLE_ENTITY,
            "La valeur 'trader' ne peut pas être vide."
        ];
    }

    /**
     * @return Generator
     * If category is SPOT, no leverage doesn't trigger error
     * If category is FUTURE, no leverage trigger error
     */
    public function getPostTradeFieldLeverageError(): Generator
    {
        $assetUuid=Utils::getAssets()[0]->getUuid()->toString();
        $categorySpot=Utils::getCategory(Category::SPOT)->getUuid()->toString();
        $categoryFuture=Utils::getCategory(Category::FUTURE)->getUuid()->toString();
        $traderUuid=Utils::getDefaultTrader()->getUuid()->toString();
        yield[
            ['type'=>Trade::TYPE_LONG, 'entry'=>12, 'neededInput'=>20, 'win'=>20, 'loss'=>29, 'asset'=>"/assets/$assetUuid", 'category'=>"/categories/$categorySpot", 'trader'=>"/users/$traderUuid"],
            Response::HTTP_CREATED
        ];
        yield[
            [
                'type'=>Trade::TYPE_LONG,
                'entry'=>12,
                'neededInput'=>20,
                'win'=>20,
                'loss'=>29,
                'asset'=>"/assets/$assetUuid",
                'category'=>"/categories/$categoryFuture",
                'trader'=>"/users/$traderUuid"],
            Response::HTTP_UNPROCESSABLE_ENTITY,
            "Le champ 'leverage' est obligatoire si vous choisissez la catégorie future."
        ];
    }

    /**
     * @param array $body
     * @param Trade $trade
     * @param int $response
     * @param string|null $errorMessage
     * @return void
     * @dataProvider getPutTradeData
     */
    public function testValidationPutTrade(
        array $body,
        Trade $trade,
        int $response,
        string $errorMessage = null
    ): void
    {
        $res=(new Put())->putTrade($body, $trade->getUuid(), $response);
        if ($response!==Response::HTTP_OK) {
            foreach ($res['violations'] as $violation) {
                self::assertStringContainsString($violation['message'], $errorMessage);
            }
        } else {
            self::assertResponseStatusCodeSame($response);
        }
    }

    public function getPutTradeData(): Generator
    {
        $trade=self::getRepository(Trade::class)->findByUserAndType(Utils::getDefaultTrader(), Trade::TYPE_LONG)[0];
        yield[['type'=>Trade::TYPE_SHORT], $trade, Response::HTTP_OK];

        $trade=self::getRepository(Trade::class)->findByUserAndStatus(Utils::getDefaultTrader(), Trade::STATUS_IN_PROGRESS)[0];
        yield[['status'=>Trade::STATUS_LOSS], $trade, Response::HTTP_OK];

        $trade=self::getRepository(Trade::class)->findByUserAndCategory(Utils::getDefaultTrader(), Category::FUTURE)[0];
        yield[['category'=>'/categories/'.Utils::getCategory(Category::SPOT)->getUuid()], $trade, Response::HTTP_OK];

        /*$trade=self::getRepository(Trade::class)->findByUserAndCategory(Utils::getDefaultTrader(), Category::SPOT)[0];
        yield[
            ['category'=>'/categories/'.Utils::getCategory(Category::FUTURE)->getUuid()],
            $trade,
            Response::HTTP_UNPROCESSABLE_ENTITY,
            "Le champ 'leverage' est obligatoire si vous choisissez la catégorie future."];*/
    }
}
